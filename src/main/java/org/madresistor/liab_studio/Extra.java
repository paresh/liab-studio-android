/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio;

import android.graphics.Color;

import java.util.Random;

public class Extra {
	public static int newColorForUser() {
		Random randGen = new Random();
		/*
		 * values[0] : Hue (0 .. 360)
		 * values[1] : Saturation (0...1)
		 * values[2] : Value (0...1)
		 */
		float hue = randGen.nextInt(360001) / 1000.0f;
		float[] values = new float[] { hue, 0.8f, 0.5f };
		return Color.HSVToColor(values);
	}

	public static double kelvinToCelsius(double value) {
		return (value - 273.15);
	}

	public static double radianToDegree(double value) {
		return (value * (180.0 / Math.PI));
	}
}
