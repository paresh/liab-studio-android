/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio;

/**
 * Ease the loading of shared object
 * One place for all shared object that need to be loaded
 */
public class JniLoader {

	/**
	 * Fake function to glue the JniLoader with the target class.
	 * call the function from static{} section of the target class.
	 */
	public static void glue() {}

	static {
		System.loadLibrary("usb-1.0");
		System.loadLibrary("box0");
		System.loadLibrary("jbox0");

		System.loadLibrary("stlport_shared");
		System.loadLibrary("muparser");

		System.loadLibrary("z");
		System.loadLibrary("png");
		System.loadLibrary("freetype2");
		System.loadLibrary("harfbuzz");
		System.loadLibrary("replot");

		System.loadLibrary("csv");

		System.loadLibrary("liab-studio");
	}
}
