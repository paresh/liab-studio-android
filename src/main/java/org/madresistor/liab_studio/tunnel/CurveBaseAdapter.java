/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.tunnel;

import android.database.DataSetObserver;
import android.widget.BaseAdapter;
import android.util.Log;

import org.madresistor.liab_studio.tunnel.Curve;
import org.madresistor.liab_studio.Config;

public abstract class CurveBaseAdapter extends BaseAdapter implements Curve.Observer {
	private final static String TAG = CurveBaseAdapter.class.getName();

	private int observer_count = 0;

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		super.registerDataSetObserver(observer);
		if (observer_count == 0) {
			Curve.registerObserver(this);
		}

		observer_count++;
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		super.unregisterDataSetObserver(observer);
		if (observer_count == 0) {
			if (Config.WARN) Log.w(TAG, "unregisterDataSetObserver() called while observer_count=0");
			return;
		}

		observer_count--;
		if (observer_count == 0) {
			Curve.unregisterObserver(this);
		}
	}

	public void onCurveChanged() {
		notifyDataSetChanged();
	}
};
