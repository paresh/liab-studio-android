/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.tunnel;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.JniLoader;

import java.util.LinkedList;
import java.io.FileDescriptor;

public class Column {
	private static final String TAG = Column.class.getName();
	public static final int SIG_DIGITS_AUTOMATIC = -1;

	/**
	 * Get the number of Columns in native.
	 * @return column count
	 */
	public native static int getColumnCount();

	/**
	 * Get the total rows in native.
	 * @return the number of rows (maximum of all rows)
	 */
	public native static int getRowCount();

	/**
	 * Remove all columns (ofcourse, and its data is automatically lost)
	 */
	public native static void removeAllColumn();

	/**
	 * Only clear all table data. (columns are retained)
	 */
	public native static void clearAllColumn();

	/**
	 * Append a column
	 * @return the Column number.
	 */
	public native static int appendColumn() throws RuntimeException;

	/**
	 * Remove the column
	 * @param column Column number
	 */
	public native static void removeColumn(int column);

	/**
	 * Clear the column cells
	 * @param column Column number
	 */
	public native static void clearColumn(int column);

	/**
	 * Get the value of a cell
	 * @param row Row number
	 * @param column Column number
	 */
	public native static float getValue(int row, int column);

	/**
	 * set the value of cell
	 * @param row Row number
	 * @param column Column number
	 * @param value Value to be set
	 */
	public native static void setValue(int row, int column, float value) throws RuntimeException;
	public static void setValue(int row, int column, double value) {
		setValue(row, column, (float) value);
	}

	/**
	 * Get the cell value as string
	 * @param row Row number
	 * @param column Column number
	 * @return cell value as String
	 */
	public native static String getValueText(int row, int column);

	/**
	 * Set the value of cell as String.
	 * The string will be converted to float value.
	 * @param row Row number
	 * @param column Column number
	 * @param text Cell value
	 */
	public native static void setValueText(int row, int column, String text);

	/**
	 * Get the number of significant digits of column
	 * @param column Column number
	 * @return SIG_DIGITS_AUTOMATIC if automatic
	 * @return significant digits
	 */
	public native static int getSigDigits(int column);

	/**
	 * Set the number of significant digits.
	 * @param column Column number
	 * @param sig_digits Significant digits.
	 * @see SIG_DIGITS_AUTOMATIC
	 */
	public native static void setSigDigits(int column, int sig_digits);

	/**
	 * Get the name of the column
	 * @param column Column number
	 * @return name of the column
	 */
	public native static String getName(int column);

	/**
	 * Set the name of the column
	 * @param column Column number
	 * @param value new column name
	 */
	public native static void setName(int column, String value);

	/**
	 * Get the unit of the column
	 * unit is a string that tell what is the mean of values. (example: Kelvin, or Kilogram)
	 * @param column Column number
	 * @return Unit as string
	 */
	public native static String getUnit(int column);

	/**
	 * Set the unit of the column
	 * @param column Column number
	 * @param value Unit as string
	 */
	public native static void setUnit(int column, String value);

	/**
	 * get color of the column
	 * Color help user distinguish between different columns easily and them eye candy.
	 * @param column Column number
	 * @return color value embedded in a 32bit int (8:8:8:8 RGBA)
	 */
	public native static int getColor(int column);

	/**
	 * set color of the column
	 * @param column Column number
	 * @param color value embedded in a 32bit int (8:8:8:8 RGBA)
	 */
	public native static void setColor(int column, int value);

	/**
	 * get line width of the column.
	 * line width is used as line width for plot when it is used as Y axis in plot.
	 * @param column Column number
	 * @return line width
	 */
	public native static int getLineWidth(int column);

	/**
	 * set line width of the column
	 * @param column Column number
	 * @param width Line width
	 */
	public native static void setLineWidth(int column, int width);

	/**
	 * Check if the column is editable or not.
	 * This will prevent user from editing the values accedently.
	 * major usecase: prevent editing of column that are readed from file.
	 * @param column Column number
	 */
	public native static boolean isEditable(int column);

	/**
	 * set the column editable
	 * @param column Column number
	 * @param editable set to true to make editable
	 */
	public native static void setEditable(int column, boolean editable);

	/**
	 * Hide the column (from user).
	 * @param column Column number
	 * @param visible true for hiding
	 */
	public native static void setVisible(int column, boolean visible);

	/**
	 * Check if the column is hidden
	 * @param column Column number
	 * @return true if hidden
	 */
	public native static boolean isVisible(int column);

	/**
	 * Get the last column value as String.
	 * usecase: get data from last cell that was added
	 * @param column Column number
	 */
	public native static String getRecentValueText(int column);

	private final static Handler m_handler = new Handler(Looper.getMainLooper());
	private final static Runnable m_notifier = new Runnable() {
		public void run() {
			if (Config.INFO) Log.i(TAG, "changed");

			for (Observer obs: m_observers) {
				obs.onColumnChanged();
			}
		}
	};

	/**
	 * Send a onColumnChanged() callback to all observer
	 */
	public static void changed() {
		m_handler.post(m_notifier);
	}

	/**
	 * Register a observer.
	 * A observer will be notified when anything in column is changed.
	 * callback will be performed on main loop
	 * @param obs Observer
	 */
	public static boolean registerObserver(Observer obs) {
		return m_observers.add(obs);
	}

	/**
	 * Unregister a observer that was previously register with registerObserver()
	 * @param obs Observer
	 */
	public static boolean unregisterObserver(Observer obs) {
		return m_observers.remove(obs);
	}

	/* afaif, LinkedList should work better than ArrayList */
	private static final LinkedList<Observer> m_observers
		= new LinkedList<Observer>();

	public interface Observer {
		public void onColumnChanged();
	}

	/**
	 * Has useful data.
	 * Generally used to check if user concent is required before clearing or deleting column.
	 * @return true if loss of data can cause greef to user.
	 */
	public static boolean hasUsefulData() {
		return getColumnCount() > 0 && getRowCount() > 0;
	}

	/**
	 * Save the content to a CSV file.
	 * @param fd file descriptor
	 * @return true on success
	 * @return false on failure
	 */
	public static native void exportCsv(FileDescriptor fd) throws RuntimeException;

	/**
	 * Import the content of CSV file
	 * @param fd file descriptor
	 * @return true on success
	 * @return false on failure
	 */
	public static native void importCsv(FileDescriptor fd) throws RuntimeException;

	static {
		JniLoader.glue();
	}
}
