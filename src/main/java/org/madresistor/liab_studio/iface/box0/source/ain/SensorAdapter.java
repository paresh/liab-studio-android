/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.ain;

import android.content.Context;

import org.madresistor.box0.module.Ain;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.iface.box0.source.Database;
import org.madresistor.liab_studio.iface.box0.source.ain.EditSensorDialog;
import org.madresistor.liab_studio.iface.box0.source.common.SensorBaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class SensorAdapter extends SensorBaseAdapter {
	List<Sensor> m_sensors = new ArrayList<Sensor>();
	private int m_count;
	private String m_fmt_unnamed;

	public SensorAdapter(Context context) {
		super(context);
		m_fmt_unnamed = context.getString(
			R.string.iface_box0_ain_unnamed_channel);
	}

	public int getCount() {
		return m_count;
	}

	public void setSelected(int index, boolean selected) {
		m_sensors.get(index).selected = selected;
	}

	public boolean isSelected(int index) {
		return m_sensors.get(index).selected;
	}

	public String getTextTop(int index) {
		String v = m_sensors.get(index).channel;
		if (v == null) {
			v = "";
		}
		return v;
	}

	public String getTextRight(int index) {
		String v = m_sensors.get(index).column;
		if (v == null) {
			v = getTextTop(index);
		}
		return v;
	}

	public String getTextLeft(int index) {
		int database_index = m_sensors.get(index).database_index;
		return Database.ain.get(database_index).name;
	}

	/* this is called when user want to edit sensor */
	public void editSensor(int index) {
		Sensor sensor = m_sensors.get(index);
		EditSensorDialog dialog = new EditSensorDialog(
			getContext(), this, sensor);
		dialog.show();
	}

	public void setModule(Ain ain) {
		if (ain == null) {
			return;
		}

		m_count = ain.count.value;
		for (int i = 0; i < m_count; i++) {
			String value;
			try {
				value = ain.label.get(i);
			} catch(Exception e) {
				value = String.format(m_fmt_unnamed, i);
			}

			if (m_sensors.size() <= i) {
				m_sensors.add(new Sensor());
			}

			Sensor sensor = m_sensors.get(i);
			sensor.channel = value;
		}

		notifyDataSetChanged();
	}

	public Sensor getSensor(int position) {
		return m_sensors.get(position);
	}

	public static class Sensor {
		boolean selected;
		String channel;
		String column;

		int database_index;
		int averaging;
		String transferFunction;
	}
}
