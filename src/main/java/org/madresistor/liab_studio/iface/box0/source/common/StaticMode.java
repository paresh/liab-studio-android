/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.common;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.madresistor.liab_studio.R;

public class StaticMode extends Spinner {
	private InternalAdapter m_adapter;

	public StaticMode(Context context) {
		super(context);
		init(context);
	}

	public StaticMode(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		m_adapter = new InternalAdapter(context);
		setAdapter(m_adapter);
		setValue(5);
	}

	public void setLimit(int limit) {
		m_adapter.setLimit(limit);
	}

	public void setValue(int value) {
		setSelection(value - 1);
	}

	public int getValue() {
		return getSelectedItemPosition() + 1;
	}

	private class InternalAdapter extends BaseAdapter {
		private int m_limit = 50;
		private final LayoutInflater m_inflater;
		private final String m_format_sps;

		public InternalAdapter(Context context) {
			m_inflater = LayoutInflater.from(context);
			Resources res = context.getResources();

			/* greater than 0 format */
			m_format_sps = res.getString(
				R.string.speed_format_sps);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = m_inflater.inflate(
					android.R.layout.simple_spinner_item , parent, false);
			}

			fillText(position, convertView);
			return convertView;
		}

		@Override
		public View getDropDownView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = m_inflater.inflate(
					android.R.layout.simple_list_item_single_choice , parent, false);
			}

			fillText(position, convertView);
			return convertView;
		}

		private void fillText(int position, View convertView) {
			TextView textView = (TextView) convertView;
			String text = String.format(m_format_sps, position + 1);
			textView.setText(text);
		}

		@Override
		public int getCount() {
			return m_limit;
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void setLimit(int limit) {
			/* assert (limit >= 0); */
			m_limit = limit;
			notifyDataSetChanged();
		}
	}
}
