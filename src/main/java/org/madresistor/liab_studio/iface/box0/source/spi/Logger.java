/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.spi;

import android.content.Context;
import android.util.Log;

import org.madresistor.box0.ResultException;
import org.madresistor.box0.driver.Driver;
import org.madresistor.box0.driver.Max31855;
import org.madresistor.box0.module.Spi;
import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.Extra;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.iface.box0.source.Database;
import org.madresistor.liab_studio.iface.box0.source.spi.SensorAdapter;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.tunnel.Curve;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Logger implements Runnable {
	private final static String TAG = Logger.class.getName();
	public static class Entry {
		public int type;
		public int col_time;
		public int[] cols_value;
		public Driver driver;

		public int sample_total, sample_got;
	};

	private final Context m_context;
	private final String m_prefix_time,
		m_prefix_x, m_prefix_y, m_prefix_z, m_prefix_value,
		m_unknown_name_format, m_curve_name_format;
	public Logger(Context context) {
		m_context = context;
		m_prefix_time = context.getString(R.string.iface_box0_column_suffix_time);
		m_prefix_value = context.getString(R.string.iface_box0_column_suffix_value);
		m_prefix_x = context.getString(R.string.iface_box0_column_suffix_x);
		m_prefix_y = context.getString(R.string.iface_box0_column_suffix_y);
		m_prefix_z = context.getString(R.string.iface_box0_column_suffix_z);
		m_unknown_name_format = context.getString(R.string.iface_box0_spi_unknown_log_name_format);
		m_curve_name_format = m_context.getString(R.string.widget_curve_name_format);
	}

	private List<Entry> m_entries = new ArrayList<Entry>();

	public int prepare(SensorAdapter adapter, Spi spi) {
		m_entries.clear(); //make sure that m_entries is empty

		for (int i = 0; i < adapter.getCount(); i++) {
			SensorAdapter.Sensor sensor = adapter.getSensor(i);
			if (sensor.selected) {
				Entry entry = prepareEntry((short) i, sensor, spi);
				if (entry == null) {
					return -1;
				}

				m_entries.add(entry);
			}
		}

		return m_entries.isEmpty() ? 0 : 1;
	}

	private Entry prepareEntry(short bSS, SensorAdapter.Sensor sensor, Spi spi) {
		Entry entry = new Entry();
		entry.type = sensor.type;

		String sensorName = buildSensorName(sensor);

		entry.col_time = Column.appendColumn();
		Column.setName(entry.col_time, sensorName + m_prefix_time);

		try {
			switch(entry.type) {
			case Database.Spi.MAX31855: {
				entry.driver = new Max31855(spi, bSS);

				int c = Column.appendColumn();
				Column.setName(c, sensorName + m_prefix_value);
				entry.cols_value = new int[] { c };
			} break;
			default:
				if (Config.WTF) Log.wtf(TAG, "entry.type (" + entry.type + ") is unknown (smells like driver missing)");
				return null;
			}
		} catch(ResultException e) {
			if (Config.ERROR) Log.e(TAG, "ResultException: " + e.toString());
			/* TODO: show error dialog */
			return null;
		}

		/* assign colors and then */
		Column.setColor(entry.col_time, Extra.newColorForUser());
		for (int c: entry.cols_value) {
			int color = Extra.newColorForUser();
			Column.setColor(c, color);

			int crv = Curve.appendCurve(entry.col_time, c);
			String name = String.format(m_curve_name_format,
				Column.getName(c),
				Column.getName(entry.col_time));
			Curve.setName(crv, name);
			Curve.setColor(crv, color);
			Curve.setLineWidth(crv, Column.getLineWidth(c));
		}

		return entry;
	}

	private String buildSensorName(SensorAdapter.Sensor sensor) {
		if (sensor.column != null && !sensor.column.isEmpty()) {
			return sensor.column;
		}

		int family_res = -1;
		switch(sensor.type) {
		case Database.Spi.MAX31855:
			family_res = R.string.max31855;
		break;
		default:
		return "";
		}

		String family = m_context.getString(family_res);
		return String.format(m_unknown_name_format, family, sensor.channel);
	}

	private final ScheduledExecutorService scheduledExecutorService =
		Executors.newSingleThreadScheduledExecutor();
	private ScheduledFuture<?> m_futureRun;
	long m_start_time;
	boolean m_infinite;
	public int start(int time, int sample_rate) {
		if (m_entries.size() == 0) {
			return 0;
		}

		int total = time * sample_rate;
		for (int i = 0; i < m_entries.size(); i++) {
			Entry entry = m_entries.get(i);
			entry.sample_total = total;
			entry.sample_got = 0;
		}

		/* log for infinite */
		m_infinite = !(time > 0);

		int period /* ms */ = 1000 / sample_rate;
		m_start_time = System.currentTimeMillis();
		m_futureRun = scheduledExecutorService.scheduleAtFixedRate(
			this, 0, period, TimeUnit.MILLISECONDS);
		return 1;
	}

	public int stop() {
		if (m_futureRun == null) {
			return -1;
		}

		return m_futureRun.cancel(true) ?
			1 : -1;
	}

	public void run() {
		int entries_skipped = 0;

		for (int i = 0; i < m_entries.size(); i++) {
			Entry entry = m_entries.get(i);

			/* task has already been cancelled */
			if (m_futureRun.isCancelled()) {
				return;
			}

			/* logging stop condition */
			if (m_infinite == false) {
				if (entry.sample_got >= entry.sample_total) {
					entries_skipped++;
					continue;
				}
			}

			long curr_time = System.currentTimeMillis();

			boolean gotError;
			try {
				gotError = logEntry(entry);
			} catch(ResultException e) {
				if (Config.ERROR) Log.e(TAG, "unable to fetch data: " + e);
				gotError = true;
			}

			if (gotError) {
				entry.sample_total--;
			} else {
				double value = (curr_time - m_start_time) / 1000.0;
				Column.setValue(entry.sample_got, entry.col_time, value);
				entry.sample_got++;
			}
		}

		if (entries_skipped == m_entries.size()) {
			if (m_futureRun != null) {
				if (!m_futureRun.cancel(true)) {
					if (Config.ERROR) Log.e(TAG, "unable to stop spi logging");
				}

				m_futureRun = null;
			}
		} else {
			/* manual change event called. to reduce the number of updates */
			Column.changed();
			Curve.changed();
		}
	}

	private boolean logEntry(Entry entry) throws ResultException {
		boolean gotError = false;

		switch(entry.type) {
		case Database.Spi.MAX31855: {
			Max31855 driver = (Max31855) entry.driver;
			double value = driver.read();
			if (Double.isNaN(value)) {
				/* some type of error occured (open?) */
				gotError = true;
			} else {
				value = Extra.kelvinToCelsius(value);
				Column.setValue(entry.sample_got, entry.cols_value[0], value);
			}
		} break;
		default:
			if (Config.WTF) Log.wtf(TAG, "spi entry.type of of range");
			gotError = true;
		break;
		}

		return gotError;
	}
}
