/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.spi;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.iface.box0.source.spi.SensorAdapter;
import org.madresistor.liab_studio.iface.box0.source.spi.SensorType;

public class EditSensorDialog extends AlertDialog
				implements DialogInterface.OnClickListener {
	private SensorAdapter m_adapter;
	private SensorAdapter.Sensor m_sensors;

	private final EditText m_column;
	private final SensorType m_type;

	public EditSensorDialog(Context context, SensorAdapter adapter, SensorAdapter.Sensor sensor) {
		super(context);
		m_sensors = sensor;
		m_adapter = adapter;

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.iface_box0_spi_sensor, null);
		setView(rootView);

		/* type */
		m_type = (SensorType) rootView.findViewById(
			R.id.iface_box0_spi_sensor_type);

		m_type.setType(sensor.type);

		/* column */
		m_column = (EditText) rootView.findViewById(
			R.id.iface_box0_spi_sensor_column);
		m_column.setText(sensor.column);

		int res_id;

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.done), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.cancel), this);

		/* title */
		setTitle(sensor.channel);

		/* title */
		setIcon(R.drawable.ic_pencil_grey600_48dp);
	}

	public void onClick(DialogInterface dialog, int which) {
		switch(which) {
		case DialogInterface.BUTTON_POSITIVE:
			m_sensors.type = m_type.getType();
			m_sensors.column = m_column.getText().toString();
			m_adapter.notifyDataSetChanged();
		break;
		case DialogInterface.BUTTON_NEUTRAL:
			dismiss();
		break;
		}
	}
}
