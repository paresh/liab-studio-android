/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.i2c;

import android.content.Context;

import org.madresistor.liab_studio.iface.box0.source.Database;
import org.madresistor.liab_studio.iface.box0.source.common.SensorBaseAdapter;
import org.madresistor.liab_studio.iface.box0.source.i2c.EditSensorDialog;

import java.util.ArrayList;
import java.util.List;

public class SensorAdapter extends SensorBaseAdapter {
	List<Sensor> m_sensors = new ArrayList<Sensor>();

	public SensorAdapter(Context context) {
		super(context);
	}

	public int getCount() {
		return m_sensors.size();
	}

	public void setSelected(int index, boolean selected) {
		m_sensors.get(index).selected = selected;
	}

	public boolean isSelected(int index) {
		return m_sensors.get(index).selected;
	}

	public String getTextTop(int index) {
		int type = m_sensors.get(index).type;
		return Database.i2c.get(type).name;
	}

	public String getTextRight(int index) {
		String v = m_sensors.get(index).column;
		if (v == null) {
			v = "";
		}
		return v;
	}

	public String getTextLeft(int index) {
		return String.format("%#x", m_sensors.get(index).address);
	}

	/* this is called when user want to edit sensor */
	public void editSensor(int index) {
		Sensor sensor = m_sensors.get(index);
		EditSensorDialog dialog = new EditSensorDialog(
			getContext(), this, sensor);
		dialog.show();
	}

	public void appendSensor(Sensor sensor) {
		m_sensors.add(sensor);
		notifyDataSetChanged();
	}

	public static class Sensor {
		boolean selected;
		int type;
		int address;
		String column;
	};

	public int[] getUsedAddresses() {
		int[] addresses = new int[m_sensors.size()];
		for (int i = 0; i < m_sensors.size(); i++) {
			addresses[i] = m_sensors.get(i).address;
		}

		return addresses;
	}

	/*  WARN: if ignore didnt match any entry, then oops will happen! */
	public int[] getUsedAddresses(Sensor ignore) {
		int used = 0;
		int[] addresses = new int[m_sensors.size() - 1];
		for (int i = 0; i < m_sensors.size(); i++) {
			Sensor curr = m_sensors.get(i);
			if (ignore != curr) {
				addresses[used++] = m_sensors.get(i).address;
			}
		}

		return addresses;
	}

	public Sensor getSensor(int position) {
		return m_sensors.get(position);
	}

	public boolean removeSensor(Sensor sensor) {
		return m_sensors.remove(sensor);
	}
}
