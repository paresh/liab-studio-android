/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import org.madresistor.liab_studio.R;

/**
 * Dialog that is shown to user when
 *   user requested start but there is nothing to start.
 */
public class NothingToStartDialog extends AlertDialog
					implements DialogInterface.OnClickListener {

	public NothingToStartDialog(Context context) {
		super(context);

		setTitle(context.getString(R.string.nothing_started));
		setMessage(context.getString(R.string.their_is_nothing_to_start));

		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.ok), this);
	}

	public void onClick(DialogInterface dialog, int which) { }
}
