/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.madresistor.box0.Device;
import org.madresistor.box0.ResultException;
import org.madresistor.liab_studio.R;

/**
 * Show device information to user.
 *  It will show name, manuf and serial of the device
 */
public class DeviceInfoDialog extends AlertDialog
					implements DialogInterface.OnClickListener {

	/**
	 * @param context Context of the activity
	 * @param dev Device who's information is to be shown
	 */
	public DeviceInfoDialog(Context context, Device dev) {
		super(context);

		String fetchError = context.getString(
			R.string.iface_box0_device_info_fetch_error);

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(R.layout.iface_box0_device_info, null);
		setView(rootView);

		TextView textView;

		/* insert name */
		textView = (TextView) rootView.findViewById(
			R.id.iface_box0_device_info_name);
		textView.setText(dev.name);

		/* insert manuf */
		textView = (TextView) rootView.findViewById(
			R.id.iface_box0_device_info_manuf);
		textView.setText(dev.manuf);

		/* insert serial */
		textView = (TextView) rootView.findViewById(
			R.id.iface_box0_device_info_serial);
		textView.setText(dev.serial);

		/* extras */
		setIcon(R.drawable.ic_information_outline_grey600_48dp);
		setTitle(context.getString(R.string.device_information));

		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.ok), this);
	}

	public void onClick(DialogInterface dialog, int which) { }
}
