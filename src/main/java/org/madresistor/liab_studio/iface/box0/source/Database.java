/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source;

import java.io.IOException;
import java.io.StringReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.AssetManager;
import android.util.Log;

import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.JniLoader;
import org.madresistor.liab_studio.R;

public class Database {
	private final static String TAG = Database.class.getName();

	public static final List<Ain> ain = new ArrayList<Ain>();
	public static final List<Spi> spi = new ArrayList<Spi>();
	public static final List<I2c> i2c = new ArrayList<I2c>();

	public static class Ain {
		public int type;
		public final static int PHOTO_GATE = 0;
		public final static int EQUATION = 1;

		public String name = "", desc = "", cat = "", func = "";
		public Parameter output = new Parameter(), input = new Parameter();


		public final class Parameter {
			public String unit = "";
			public double max = 0, min = 0;

			public Parameter(Object parser) {
				/* TODO: parse from parser */
			}

			public Parameter() {

			}
		}

		/* TODO: support other entries */

		public Ain(Object parser) {
			/* TODO: parse from parser */
		}

		public Ain() {

		}

		public static final float PHOTO_GATE_THRESHOLD = 1.5f;

		static public Ain getPhotoGate(Resources res) {
			Ain ain = new Ain();
			ain.type = PHOTO_GATE;
			ain.name = res.getString(R.string.photo_gate);
			ain.cat = res.getString(R.string.time);
			ain.output.unit = res.getString(R.string.voltage);
			ain.output.max = 3;
			ain.output.min = 0;
			ain.input.unit = res.getString(R.string.trigger);
			return ain;
		}

		static public Ain getCustomEquation(Resources res) {
			Ain ain = new Ain();
			ain.type = EQUATION;
			ain.name = res.getString(R.string.custom_equation);
			return ain;
		}
	}

	public static class Spi {
		public final int type;
		public final String name;

		public final static int
			MAX31855 = 0;

		public Spi(int type, String name) {
			this.type = type;
			this.name = name;
		}
	}

	public static class I2c {
		public static class Address {
			public final int value;
			public final String name;
			public Address(int value, String name) {
				this.value = value;
				this.name = name;
			}
		}

		public final static int
			ADXL345 = 0,
			BMP180 = 1,
			TMP006 = 2,
			TSL2591 = 3,
			LM75 = 4,
			L3GD20 = 5,
			SI114X = 6;

		public final int type;
		public final String name;
		public final Address[] addresses;
		public I2c(int type, String name, Address... addresses) {
			this.type = type;
			this.name = name;
			this.addresses = addresses;
		}
	}

	public static void parse(String file, AssetManager assets) {
		try {
			parse(assets.open(file));
		}catch(IOException e) {
			if (Config.WARN) Log.w(TAG, "unable to open " +  file + " from assets");
		}
	}

	public static void parse(InputStream input) {
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(input, null);
			parse(parser);
		} catch(/* XmlPullParserException|IOException */ Exception e) {
			if (Config.WTF) Log.wtf(TAG, "unable to prepare parser", e);
		}

	}

	public static void parse(XmlPullParser parser) {
		try {
			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {
					String value = parser.getName();
					if (Config.INFO) Log.i(TAG, "tag start: " + value);
				} else if (eventType == XmlPullParser.TEXT){
					String value = parser.getText();
					if (Config.INFO) Log.i(TAG, "text part: " + value);
				} else if (eventType == XmlPullParser.END_TAG) {
					String value = parser.getName();
					if (Config.INFO) Log.i(TAG, "tag end: " + value);
				} else if (eventType == XmlPullParser.START_DOCUMENT) {
					if (Config.INFO) Log.i(TAG, "start of document");
				}

				eventType = parser.next();
			}
		} catch(/* XmlPullParserException|IOException */ Exception e) {
			if (Config.WTF) Log.wtf(TAG, "error while parsing", e);
		}
	}

	private static boolean prepared = false;
	public static void prepare(Context context) {
		if (prepared) {
			if (Config.WARN) Log.w(TAG, "already prepared");
			return;
		}
		prepared = true;

		Resources res = context.getResources();

		ain.add(Ain.getCustomEquation(res));
		ain.add(Ain.getPhotoGate(res));

		//parse("box0-sensors.xml", res.getAssets());


		/* ========================================================== */

		i2c.add(new I2c(I2c.ADXL345, res.getString(R.string.adxl345),
			new I2c.Address(0x53, res.getString(R.string.adxl345_53)),
			new I2c.Address(0x1D, res.getString(R.string.adxl345_1D))
		));

		i2c.add(new I2c(I2c.BMP180, res.getString(R.string.bmp180),
			new I2c.Address(0x77, res.getString(R.string.hard_wired))
		));

		i2c.add(new I2c(I2c.TMP006, res.getString(R.string.tmp006),
			new I2c.Address(0x40, res.getString(R.string.tmp006_40)),
			new I2c.Address(0x41, res.getString(R.string.tmp006_41)),
			new I2c.Address(0x42, res.getString(R.string.tmp006_42)),
			new I2c.Address(0x43, res.getString(R.string.tmp006_43)),
			new I2c.Address(0x44, res.getString(R.string.tmp006_44)),
			new I2c.Address(0x45, res.getString(R.string.tmp006_45)),
			new I2c.Address(0x46, res.getString(R.string.tmp006_46)),
			new I2c.Address(0x47, res.getString(R.string.tmp006_47))
		));

		i2c.add(new I2c(I2c.TSL2591, res.getString(R.string.tsl2591),
			new I2c.Address(0x29, res.getString(R.string.hard_wired))
		));

		i2c.add(new I2c(I2c.LM75, res.getString(R.string.lm75),
			new I2c.Address(0x48, res.getString(R.string.lm75_48)),
			new I2c.Address(0x49, res.getString(R.string.lm75_49)),
			new I2c.Address(0x4A, res.getString(R.string.lm75_4A)),
			new I2c.Address(0x4B, res.getString(R.string.lm75_4B)),
			new I2c.Address(0x4C, res.getString(R.string.lm75_4C)),
			new I2c.Address(0x4D, res.getString(R.string.lm75_4D)),
			new I2c.Address(0x4E, res.getString(R.string.lm75_4E)),
			new I2c.Address(0x4F, res.getString(R.string.lm75_4F))
		));

		i2c.add(new I2c(I2c.L3GD20, res.getString(R.string.l3gd20),
			new I2c.Address(0x6A, res.getString(R.string.l3gd20_6A)),
			new I2c.Address(0x6B, res.getString(R.string.l3gd20_6B))
		));

		/* hard wired, though configurable using command */
		i2c.add(new I2c(I2c.SI114X, res.getString(R.string.si114x),
			new I2c.Address(0x60, res.getString(R.string.hard_wired))
		));

		/* ========================================================== */

		spi.add(new Spi(Spi.MAX31855, res.getString(R.string.max31855)));
	}

	/**
	 * Test if transfer function is valid.
	 * @param eq Transfer function
	 * @return null on success
	 * @return error message on failure
	 */
	public final static native String transferFuncTest(String eq);

	static {
		JniLoader.glue();
	}
}
