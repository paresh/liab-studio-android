/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.spi;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.linearlistview.LinearListView;
import org.madresistor.box0.Device;
import org.madresistor.box0.ResultException;
import org.madresistor.box0.module.Spi;
import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.iface.box0.source.Source;
import org.madresistor.liab_studio.iface.box0.source.common.StaticMode;
import org.madresistor.liab_studio.iface.box0.source.spi.Logger;

public class SpiSource extends Source {
	private final static String TAG = SpiSource.class.getName();
	private SensorAdapter m_sensorAdapter;
	private StaticMode m_staticMode;
	private Spi m_spi;
	private final Logger m_logger;

	public SpiSource(Context context) {
		super(context);
		m_sensorAdapter = new SensorAdapter(context);
		m_logger = new Logger(context);
	}

	public int setTime(int time) {
		if (time < 0) {
			return m_logger.stop();
		}

		if (m_spi == null) {
			if (Config.WARN) Log.w(TAG, "m_spi is null");
			return -1;
		}

		int status = m_logger.prepare(m_sensorAdapter, m_spi);
		if (status <= 0) {
			return status;
		}

		int sample_rate = m_staticMode.getValue();
		return m_logger.start(time, sample_rate);
	}

	public void setDevice(Device dev) {
		close();
		if (dev != null) {
			open(dev);
		}

		update();
	}

	private void close() {
		if (m_spi != null) {
			try {
				m_spi.close();
			} catch (ResultException e) {

			} finally {
				m_spi = null;
			}
		}
	}

	private void open(Device dev) {
		try {
			m_spi = dev.spi(0);
		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "Unable to open module");
			m_spi = null;
		}
	}

	public void extractView(View rootView) {
		if (rootView == null) {
			m_staticMode = null;
			return;
		}

		LinearListView sensorTable = (LinearListView) rootView.findViewById(
			R.id.iface_box0_spi_sensor_table);
		m_sensorAdapter.attachTo(sensorTable);

		m_staticMode = (StaticMode) rootView.findViewById(
			R.id.iface_box0_spi_static_mode);

		update();
	}

	private void update() {
		m_sensorAdapter.setModule(m_spi);
	}
}
