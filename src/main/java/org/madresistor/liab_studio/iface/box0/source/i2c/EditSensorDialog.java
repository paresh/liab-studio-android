/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.i2c;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.iface.box0.source.i2c.SensorAdapter;
import org.madresistor.liab_studio.iface.box0.source.i2c.SensorTypeAndAddress;

//TODO: think and if ok, implement ExpandableListView
//  instead of providing seperate ListView for address and type
public class EditSensorDialog extends AlertDialog
				implements DialogInterface.OnClickListener {
	private final static String TAG = EditSensorDialog.class.getName();
	private final SensorAdapter m_adapter;
	private final SensorAdapter.Sensor m_sensor;

	private final SensorTypeAndAddress m_typeAndAddress;
	private final EditText m_column;

	public EditSensorDialog(Context context, SensorAdapter adapter, SensorAdapter.Sensor sensor) {
		super(context);
		m_sensor = sensor;
		m_adapter = adapter;

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.iface_box0_i2c_sensor, null);
		setView(rootView);

		/* type */
		m_typeAndAddress = (SensorTypeAndAddress) rootView.findViewById(
			R.id.iface_box0_i2c_sensor_type_and_address);

		int[] list = adapter.getUsedAddresses(sensor);
		m_typeAndAddress.setUsedAddresses(list, sensor.address);
		m_typeAndAddress.setType(sensor.type);
		m_typeAndAddress.setAddress(sensor.address);

		/* column */
		m_column = (EditText) rootView.findViewById(
			R.id.iface_box0_i2c_sensor_column);
		m_column.setText(sensor.column);

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.done), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.cancel), this);

		/* negative button */
		setButton(DialogInterface.BUTTON_NEGATIVE,
			context.getString(R.string.delete), this);

		/* title */
		setTitle(R.string.edit);

		/* title */
		setIcon(R.drawable.ic_pencil_grey600_48dp);
	}

	public void onClick(DialogInterface dialog, int which) {
		switch(which) {
		case DialogInterface.BUTTON_POSITIVE:
			m_sensor.type = m_typeAndAddress.getType();
			m_sensor.address = m_typeAndAddress.getAddress();
			m_sensor.column = m_column.getText().toString();
			m_adapter.notifyDataSetChanged();
		break;
		case DialogInterface.BUTTON_NEGATIVE:
			boolean removed = m_adapter.removeSensor(m_sensor);
			if (removed == false) {
				if (Config.WARN) Log.w(TAG, "unable to remove sensor");
			}
			m_adapter.notifyDataSetChanged();
		break;
		}
	}
}
