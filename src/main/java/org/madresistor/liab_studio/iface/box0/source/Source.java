/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source;

import android.content.Context;
import android.view.View;

import org.madresistor.box0.Device;

public abstract class Source {
	private final Context m_context;
	public Source(Context context) {
		m_context = context;
	}

	public Context getContext() {
		return m_context;
	}

	/**
	 * @param time [-1 = stop], [0 = infinite duration], [+ve for seconds duration]
	 * @return -ve means error
	 * @return 0 means not enabled
	 * @return +ve mean success
	 */
	public abstract int setTime(int time);

	/**
	 * Box0 has more than one source.
	 * so, Source can open the right libbox0 module from device.
	 * dev = null is passed to force libbox0 module close.
	 */
	public abstract void setDevice(Device dev);

	/**
	 * Box0 has more than 1 source,
	 *   so the source has to extract ui elements that it need to act upon.
	 *
	 * when rootView is created (onCreateView), view=rootView is passed.
	 * when rootView is distroyed (onDistroyView), view=null is passed.
	 *
	 * @param view rootView
	 */
	public abstract void extractView(View view);
}
