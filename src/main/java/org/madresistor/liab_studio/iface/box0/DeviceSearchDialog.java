/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.app.AlertDialog;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.iface.box0.DeviceManager;

/**
 * Device search dialog.
 *  It is used for multiple purpose.
 */
public class DeviceSearchDialog extends AlertDialog
		implements DialogInterface.OnClickListener {
	private final DeviceManager m_deviceManager;

	/**
	 * @param DeviceManager device manager
	 * @param context Context of the activity
	 * @param title_res_id Title resource ID.
	 */
	public DeviceSearchDialog(DeviceManager deviceManager, Context context,
							int title_res_id) {
		super(context);

		m_deviceManager = deviceManager;
		setIcon(R.drawable.ic_usb_grey600_48dp);

		Resources res = context.getResources();
		setTitle(res.getString(title_res_id));
		setMessage(res.getString(R.string.try_again));
		setButton(DialogInterface.BUTTON_POSITIVE,
			res.getString(R.string.retry), this);
	}

	public void onClick(DialogInterface dialog, int which) {
		new Handler().post(new Runnable() {
			public void run() {
				m_deviceManager.search();
			}
		});
	}
}
