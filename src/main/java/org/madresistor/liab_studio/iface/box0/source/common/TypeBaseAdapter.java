/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/* a generic Type manager for spinner in Sensor Types */
public abstract class TypeBaseAdapter extends BaseAdapter {
	private final LayoutInflater m_inflater;

	public TypeBaseAdapter(Context context) {
		m_inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getView(android.R.layout.simple_spinner_item,
			position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getView(android.R.layout.simple_spinner_dropdown_item,
			position, convertView, parent);
	}

	private View getView(int layout, int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = m_inflater.inflate(layout, parent, false);
		}

		String name = getItem(position).toString();
		TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
		textView.setText(name);

		return convertView;
	}

	/*
	@Override
	public Object getItem(int arg0) {
		return arg0;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	*/

	// public long getItemId(int position) will return the id of the type
	// public Object getItem(int arg0)  this will return the type name
	// public int getCount() will return the different type of sensors supported
}
