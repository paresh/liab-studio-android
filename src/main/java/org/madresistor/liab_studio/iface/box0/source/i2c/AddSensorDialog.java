/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.i2c;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.iface.box0.source.i2c.SensorAdapter;
import org.madresistor.liab_studio.iface.box0.source.i2c.SensorTypeAndAddress;

//TODO: think and if ok, implement ExpandableListView
//  instead of providing seperate ListView for address and type
public class AddSensorDialog extends AlertDialog
				implements DialogInterface.OnClickListener {
	private final SensorAdapter m_adapter;

	public AddSensorDialog(Context context, SensorAdapter adapter) {
		super(context);
		m_adapter = adapter;

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.iface_box0_i2c_sensor, null);
		setView(rootView);

		SensorTypeAndAddress typeAndAddress = (SensorTypeAndAddress)
			rootView.findViewById(R.id.iface_box0_i2c_sensor_type_and_address);

		typeAndAddress.setUsedAddresses(adapter.getUsedAddresses(), -1);

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.add), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.discard), this);

		/* title */
		setTitle(context.getString(R.string.i2c));

		/* title */
		setIcon(R.drawable.ic_plus_grey600_48dp);
	}

	public void onClick(DialogInterface dialog, int which) {
		switch(which) {
		case DialogInterface.BUTTON_POSITIVE: {
			SensorTypeAndAddress typeAndAddress = (SensorTypeAndAddress) findViewById(
				R.id.iface_box0_i2c_sensor_type_and_address);
			EditText column = (EditText) findViewById(
				R.id.iface_box0_i2c_sensor_column);

			SensorAdapter.Sensor sensor = new SensorAdapter.Sensor();
			sensor.selected = true;
			sensor.column = column.getText().toString();
			sensor.type = typeAndAddress.getType();
			sensor.address = typeAndAddress.getAddress();

			m_adapter.appendSensor(sensor);
		} break;
		case DialogInterface.BUTTON_NEUTRAL:
			dismiss();
		break;
		}
	}
}
