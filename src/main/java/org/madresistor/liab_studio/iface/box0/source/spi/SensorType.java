/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0.source.spi;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

import org.madresistor.liab_studio.iface.box0.source.Database;
import org.madresistor.liab_studio.iface.box0.source.common.TypeBaseAdapter;

public class SensorType extends Spinner {
	private Adapter m_adapter;
	public SensorType(Context context) {
		super(context);
		init(context);
	}

	public SensorType(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		m_adapter = new Adapter(context);
		setAdapter(m_adapter);
	}

	/* NOTE: currently type and index are same, so doing a search is just waste */

	public void setType(int type) {
		setSelection(type);
	}

	public int getType() {
		return getSelectedItemPosition();
	}

	private class Adapter extends TypeBaseAdapter {
		public Adapter(Context context) {
			super(context);
		}

		@Override
		public Object getItem(int position) {
			return Database.spi.get(position).name;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public int getCount() {
			return Database.spi.size();
		}
	}
}
