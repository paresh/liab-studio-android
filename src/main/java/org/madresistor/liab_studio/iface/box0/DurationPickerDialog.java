/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface.box0;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.iface.box0.Box0;

/**
 * Duration picker for user.
 */
public class DurationPickerDialog extends AlertDialog implements
				DialogInterface.OnClickListener,
				NumberPicker.OnValueChangeListener,
				CompoundButton.OnCheckedChangeListener {

	SharedPreferences m_prefs;
	private final SwitchCompat m_autoStop;
	private final LinearLayout m_inputs;
	private final NumberPicker m_hour, m_min, m_sec;

	public DurationPickerDialog(Context context) {
		super(context);

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.iface_box0_duration_picker, null);
		setView(rootView);

		m_autoStop = (SwitchCompat) rootView.findViewById(
			R.id.iface_box0_duration_picker_auto_stop);
		m_autoStop.setOnCheckedChangeListener(this);

		m_inputs = (LinearLayout) rootView.findViewById(
			R.id.iface_box0_duration_picker_inputs);

		m_hour = (NumberPicker) rootView.findViewById(
			R.id.iface_box0_duration_picker_hour);
		m_hour.setMinValue(0);
		m_hour.setMaxValue(99);
		m_hour.setOnValueChangedListener(this);

		m_min = (NumberPicker) rootView.findViewById(
			R.id.iface_box0_duration_picker_min);
		m_min.setMinValue(0);
		m_min.setMaxValue(99);
		m_min.setOnValueChangedListener(this);

		m_sec = (NumberPicker) rootView.findViewById(
			R.id.iface_box0_duration_picker_sec);
		m_sec.setMinValue(0);
		m_sec.setMaxValue(99);
		m_sec.setOnValueChangedListener(this);

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.done), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.cancel), this);

		/* title */
		setTitle(R.string.duration);

		/* title */
		setIcon(R.drawable.ic_av_timer_grey600_48dp);

		/* sync preference with inputs */
		m_prefs = context.getSharedPreferences(
			Box0.PREFS_NAME, 0);
	}

	/**
	 * when the Dialog start, update the value of m_autoStop.
	 *  it was not possible to perform the work inside constructor
	 */
	@Override
	protected void onStart() {
		super.onStart();

		boolean autoStop =
			m_prefs.getBoolean(Box0.PREF_AUTO_STOP, Box0.DEF_AUTO_STOP);
		m_autoStop.setChecked(autoStop);
	}

	/**
	 * called when m_autoStop is checked.
	 *  if autoStop is checked, it will show the input to enter duration.
	 *  if not, it will hide them from user
	 */
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		boolean autoStop = isChecked;

		getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);

		/* update inputs */
		m_inputs.setVisibility(autoStop ? View.VISIBLE : View.GONE);

		if (!autoStop) {
			return;
		}

		/* inject values in inputs */
		int hour = m_prefs.getInt(Box0.PREF_DURATION_HOUR, Box0.DEF_DURATION_HOUR);
		int min = m_prefs.getInt(Box0.PREF_DURATION_MIN, Box0.DEF_DURATION_MIN);
		int sec = m_prefs.getInt(Box0.PREF_DURATION_SEC, Box0.DEF_DURATION_SEC);

		m_hour.setValue(hour);
		m_min.setValue(min);
		m_sec.setValue(sec);
	}

	/**
	 * Called when value of m_hour or m_min or m_sec is changed.
	 *  all there NumberPicker share the same callback.
	 */
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		int hour = m_hour.getValue();
		int min = m_min.getValue();
		int sec = m_sec.getValue();

		boolean enabled = ((hour | min | sec) != 0);
		getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(enabled);
	}

	public void onClick(DialogInterface dialog, int which) {
		switch(which) {
		case DialogInterface.BUTTON_POSITIVE:
			SharedPreferences.Editor editor = m_prefs.edit();

			boolean autoStop = m_autoStop.isChecked();
			editor.putBoolean(Box0.PREF_AUTO_STOP, autoStop);

			if (autoStop) {
				m_hour.clearFocus();
				m_min.clearFocus();
				m_sec.clearFocus();

				int hour = m_hour.getValue();
				int min = m_min.getValue();
				int sec = m_sec.getValue();

				editor.putInt(Box0.PREF_DURATION_HOUR, hour);
				editor.putInt(Box0.PREF_DURATION_MIN, min);
				editor.putInt(Box0.PREF_DURATION_SEC, sec);
			}

			editor.apply();
		break;
		case DialogInterface.BUTTON_NEUTRAL:
			dismiss();
		break;
		}
	}
}
