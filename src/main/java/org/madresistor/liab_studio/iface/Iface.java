/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.iface;

import android.content.Context;

import org.madresistor.liab_studio.Module;

import java.util.ArrayList;
import java.util.List;

/**
 * A Iface is a special module that fetch data. (Producer Module)
 */
public abstract class Iface extends Module {
	public Iface(Context context) {
		super(context);
	}

	/**
	 * Set a onStatusChangedListener to the Iface.
	 * The underlying iface should implement and keep a reference to listener for future.
	 * @param listener Listener
	 */
	public abstract void setOnStatusChangedListener(OnStatusChangedListener listener);

	/**
	 * It is used to listen to Iface status information by MainActivity
	 */
	public interface OnStatusChangedListener {
		public final static int
		/** Iface has stopped */
			STOP = -1,

		/** Iface is logging for infinite duration */
			INFINITE = 0;

		/**
		 * This function tell the listener about the state of the iface.
		 *  Currently, this is used for progress bar.
		 *
		 * @param iface caller interface
		 * @param value
		 *  if (value > 0) then interface is logging for "value" seconds duration.
		 *
		 *  if (value == OnStatusChangedListener.STOP)
		 *   then interface has stopped logging
		 *
		 *  if (value ==  OnStatusChangedListener.INFINITE)
		 *   then interface is logging for infinite duration.
		 */
		public void onStatusChanged(Iface iface, int value);
	}

	/**
	 * All intefce should subclass this Frontend.
	 * currently nothing is overrided, but for future, we may
	 */
	public abstract class Frontend extends Module.Frontend {

	}

	public final static List< Class<Iface> > registered = new ArrayList< Class<Iface> >();
	protected static void register(Class<? extends Iface> iface) {
		registered.add((Class<Iface>) iface);
	}
}
