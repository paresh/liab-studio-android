/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.GridView;
import android.widget.AdapterView;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;

/**
 * The dialog that is shown to user that let it
 *  choose between different modules available.
 */
public class ModuleSelector extends AlertDialog {
	/**
	 * Construct a Module object
	 * @param context The activity context
	 * @param listener The object that listen to the selected module
	 */
	public ModuleSelector(Context context, OnModuleSelectedListener listener) {
		super(context);

		LayoutInflater inflater = LayoutInflater.from(context);
		View rootView = inflater.inflate(R.layout.module_selector, null);

		GridView list;

		/* widgets */
		list = (GridView) rootView.findViewById(R.id.module_selector_widget);
		list.setAdapter(new ImageAdapter(context, R.array.widgets_img));
		registerCallback(list, listener, OnModuleSelectedListener.WIDGET);

		/* interface */
		list = (GridView) rootView.findViewById(R.id.module_selector_interface);
		list.setAdapter(new ImageAdapter(context, R.array.interfaces_img));
		registerCallback(list, listener, OnModuleSelectedListener.INTERFACE);

		setView(rootView);
	}

	private static class ImageAdapter extends BaseAdapter {
		private final Context m_context;
		private final TypedArray m_resource;

		/**
		 * @param context Activity context
		 * @param array_res_id Array resource ID
		 */
		public ImageAdapter(Context context, int array_res_id) {
			m_context = context;
			m_resource = context.getResources().obtainTypedArray(array_res_id);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView i;
			if (convertView == null) {
				i = new ImageView(m_context);
			} else {
				i = (ImageView) convertView;
			}

			i.setImageDrawable(m_resource.getDrawable(position));

			return i;
		}

		public int getCount() {
			return m_resource.length();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}
	}

	/**
	 * Register a callback to the list that listen to list view selected item
	 *  and route it to the listener
	 * @param list linear list view
	 * @param listener module selected listener
	 * @param type module type
	 *    (OnModuleSelectedListener.WIDGET or OnModuleSelectedListener.INTERFACE)
	 */
	private void registerCallback(GridView list,
			final OnModuleSelectedListener listener, final int type) {
		list.setOnItemClickListener(new GridView.OnItemClickListener() {
			@Override
			public void onItemClick (AdapterView<?> gridView, View view,
				int position, long id) {
				dismiss();
				listener.onModuleSelected(type, position);
			}
		});
	}

	/**
	 * This is the listener that will route back
	 *  the information on which module has been selected
	 */
	public interface OnModuleSelectedListener {

		/* types */
		public final static int
			WIDGET = 0,
			INTERFACE = 1;

		/* WIDGET subtypes.
		 * These values match with the index of the string list in resources.
		 */
		public final static int
			GRAPH = 0,
			TABLE = 1,
			RECENT = 2;

		/* INTEFACE subtypes.
		 * These values match with the index of the string list in resources.
		 */
		public final static int
			BOX0 = 0;

		/**
		 * @param type module type
		 * @param sub_type subtype value
		 */
		public void onModuleSelected(int type, int sub_type);
	}
}
