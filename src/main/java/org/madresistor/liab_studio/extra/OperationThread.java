/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.extra;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

/**
 * A Operation Thread.
 * Similar to AsycTask, but AsycTask is documented to not run
 *  thread that can take more than a second or so.
 * Also, generally a operation thread need to perform some operation and
 *  give the feed to user.
 * for success feedback, we have successToast()
 *  and for failed failedDialog().
 */
public class OperationThread extends Thread {
	public OperationThread(String name) {
		super(name);
	}

	/**
	 * Show a success toast to User (on UI thread)
	 * @param context Activity context
	 * @param msg Messaget ot show on Toast.
	 */
	public static void successToast(final Context context, final String msg) {
		Handler h = new Handler(context.getMainLooper());
		h.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
			}
		});
	}

	/**
	 * Show failed Dialog to user (on UI thread)
	 * @param context Activity context
	 * @param e Exception that was raised during operation
	 */
	public static void failedDialog(final Context context, final Exception e) {
		Handler h = new Handler(context.getMainLooper());
		h.post(new Runnable() {
			@Override
			public void run() {
				OperationFailedDialog dialog =
					new OperationFailedDialog(context, e);
				dialog.show();
			}
		});
	}
}
