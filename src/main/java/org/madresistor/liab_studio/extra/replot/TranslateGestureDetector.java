/*
 * This file is part of libreplot.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (C) 2010 The Android Open Source Project
 * Licence: Apache Licence 2.0
 * URL: http://android-developers.blogspot.in/2010/06/making-sense-of-multitouch.html
 * Source: https://code.google.com/p/android-touchexample/
 */

package org.madresistor.liab_studio.extra.replot;

import android.content.Context;
import android.view.MotionEvent;

/**
 * Translation detector, like when
 *  - user place finger
 *  - move the finger while touching the screen
 *  - lift finger
 */
public class TranslateGestureDetector {
	private final OnTranslateListener m_listener;

	private float m_lastTouchX;
	private float m_lastTouchY;
	private static final int INVALID_POINTER_ID = -1;
	private int m_activePointerId = INVALID_POINTER_ID;

	public TranslateGestureDetector(Context context, OnTranslateListener listener) {
		/* currently context is not used in here */
		m_listener = listener;
	}

	public boolean onTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();
		switch (action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: {
			final float x = ev.getX();
			final float y = ev.getY();
			m_activePointerId = ev.getPointerId(0);
			m_listener.onStart(x, y);
			m_lastTouchX = x;
			m_lastTouchY = y;
		} break;

		case MotionEvent.ACTION_MOVE: {
			final int pointerIndex = ev.findPointerIndex(m_activePointerId);
			final float x = ev.getX(pointerIndex);
			final float y = ev.getY(pointerIndex);

			/* Only move if the ScaleGestureDetector isn't processing a gesture. */
			if (!m_listener.isScaleInProgress()) {
				final float dx = x - m_lastTouchX;
				final float dy = y - m_lastTouchY;
				m_listener.onPerform(dx, dy);
			}

			m_lastTouchX = x;
			m_lastTouchY = y;
		} break;

		case MotionEvent.ACTION_UP: {
			m_listener.onStop(ev.getX(), ev.getY());
			m_activePointerId = INVALID_POINTER_ID;
		} break;

		case MotionEvent.ACTION_CANCEL: {
			m_listener.onStop(ev.getX(), ev.getY());
			m_activePointerId = INVALID_POINTER_ID;
		} break;

		case MotionEvent.ACTION_POINTER_UP: {
			final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK)
					>> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
			final int pointerId = ev.getPointerId(pointerIndex);
			if (pointerId == m_activePointerId) {
				/* This was our active pointer going up. Choose a new
				 * active pointer and adjust accordingly.
				 */
				final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
				m_lastTouchX = ev.getX(newPointerIndex);
				m_lastTouchY = ev.getY(newPointerIndex);
				m_activePointerId = ev.getPointerId(newPointerIndex);
			}
		} break;
		}

		return true;
	}

	public interface OnTranslateListener {
		/**
		 * when translation start.
		 * @param x X coordinate
		 * @param y Y coordinate
		 */
		void onStart(float x, float y);

		/**
		 * When translation is performed.
		 * This method is called multiple times.
		 * @param dx X coordinate change
		 * @param dy Y coordinate change
		 */
		void onPerform(float dx, float dy);

		/**
		 * When the user stop the translation. (lift up the finger.)
		 * @param x X coordinate
		 * @param y Y coordinate
		 */
		void onStop(float x, float y);

		/**
		 * get true if scaling is in progress.
		 * This is used to prevent scaling event mistakenly considered as translation.
		 * @return true if scaling is in progress
		 */
		boolean isScaleInProgress();
	}
}
