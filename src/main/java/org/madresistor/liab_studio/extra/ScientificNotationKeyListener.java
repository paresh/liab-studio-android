/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.extra;

import android.text.InputType;
import android.text.Spanned;
import android.text.method.NumberKeyListener;

public class ScientificNotationKeyListener extends NumberKeyListener {
	private static final char[] m_acceptedChars = {
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'.', '+', '-', 'E', 'e'
	};

	@Override
	protected char[] getAcceptedChars() {
		return m_acceptedChars;
	}

	@Override
	public int getInputType() {
		return InputType.TYPE_NUMBER_FLAG_SIGNED |
			InputType.TYPE_CLASS_NUMBER |
			InputType.TYPE_NUMBER_FLAG_DECIMAL;
	}

	/*
	 * symbol: exp: 'e' OR 'E'
	 * symbol: sign: '+' OR '-'
	 * symbol: decimal: '.'
	 * symbol: number: '0' OR '1' OR '2' OR '3' OR '4'
	 *                 OR '5' OR '6' OR '7' OR '8' OR '9'
	 *
	 * rule1: multiple `exp' cannot exists
	 * rule2: muliple `sign' cannot exists (until seperated by `exp')
	 * rule3: `sign' can only occur at position 0 and just after  `exp'
	 * rule4: multiple `decimal' cannot occur (until seperated by `exp')
	 *
	 * TODO: strip unwanted characters instead of reject all characters
	 * TODO: reject illegal forms
	 * TODO: design better algorithm
	 */
	@Override
	public CharSequence filter(CharSequence source, int start, int end,
							   Spanned dest, int dstart, int dend) {
		CharSequence out = super.filter(source, start, end, dest, dstart, dend);

		if (out != null) {
			source = out;
			start = 0;
			end = out.length();
		}

		StringBuilder resultBuilder = new StringBuilder();
		resultBuilder = resultBuilder.append(dest.subSequence(0, dstart));
		resultBuilder = resultBuilder.append(source.subSequence(start, end));
		resultBuilder = resultBuilder.append(dest.subSequence(dend, dest.length()));
		String result = resultBuilder.toString();

		boolean gotExponent = false;
		boolean gotSign = false;
		boolean gotDecimalPoint = false;
		boolean gotNumber = false;
		for (int i = 0; i < result.length(); i++) {
			char ch = result.charAt(i);
			if (isExponentChar(ch)) {
				/* rule1 */
				if (gotExponent) {
					return "";
				}

				gotExponent = true;
				gotSign = false;
				gotDecimalPoint = false;
				gotNumber = false;
			} else if (isSignChar(ch)) {
				/* rule2, rule3 */
				if (i > 0 && (gotNumber | gotSign | gotDecimalPoint)) {
					return "";
				}

				gotSign = true;
			} else if (isDecimalPointChar(ch)) {
				/* rule4 */
				if (gotDecimalPoint) {
					return "";
				}

				gotDecimalPoint = true;
			} else {
				gotNumber = true;
			}
		}

		return out;
	}

	private static boolean isExponentChar(final char c) {
		return c == 'e' || c == 'E';
	}

	private static boolean isSignChar(final char c) {
		return c == '-' || c == '+';
	}

	// TODO: Needs internationalization
	private static boolean isDecimalPointChar(final char c) {
		return c == '.';
	}
}
