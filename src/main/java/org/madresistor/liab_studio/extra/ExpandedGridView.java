/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.extra;

import android.content.Context;
import android.view.View;
import android.widget.GridView;
import android.util.AttributeSet;

/* ref: http://stackoverflow.com/a/28898401/1500988 */

/**
 * When more items are added to the GridView,
 * it will just expand its height,
 * as opposed to keeping its height set and using scrolling
 */
public class ExpandedGridView extends GridView {
	public ExpandedGridView(Context context) {
		super(context);
	}

	public ExpandedGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ExpandedGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(
			MEASURED_SIZE_MASK, View.MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
