/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.extra.filepicker;

import com.nononsenseapps.filepicker.FilePickerFragment;

import android.support.annotation.NonNull;
import android.os.Bundle;

import java.io.File;
import java.util.ArrayList;

/**
 * Generic class that accept a list of extension to match against.
 *  Only those item are shown that are accepted by super (currently check hidden or not)
 *   and pass the extension match]
 */
public class ExtFilteredFilePickerFragment extends FilePickerFragment {

	public final static String KEY_ALLOWED_EXT = "KEY_ALLOWED_EXT";

	private ArrayList<String> m_allowedExt;

	/**
	 * same as setArgs() without allowedExt
	 * @param allowedExt list of extensions that are allowed
	 */
	public void setArgs(final String startPath, final int mode,
						final boolean allowMultiple, final boolean allowDirCreate,
						final ArrayList<String> allowedExt) {
		super.setArgs(startPath, mode, allowMultiple, allowDirCreate);

		/* piggyback our too... */
		Bundle b = getArguments();
		if (allowedExt != null) {
			b.putStringArrayList(KEY_ALLOWED_EXT, allowedExt);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// Only if we have no state
		if (mCurrentPath == null) {
			if (savedInstanceState != null) {
				m_allowedExt = savedInstanceState.getStringArrayList(KEY_ALLOWED_EXT);
			} else if (getArguments() != null) {
				m_allowedExt = getArguments().getStringArrayList(KEY_ALLOWED_EXT);
			}
		}

		super.onActivityCreated(savedInstanceState);
    }

	@Override
	public void onSaveInstanceState(Bundle b) {
		super.onSaveInstanceState(b);
		b.putStringArrayList(KEY_ALLOWED_EXT, m_allowedExt);
	}

	/**
	 *
	 * @param file
	 * @return The file extension. If file has no extension, it returns null.
	 */
	private String getExtension(@NonNull File file) {
		String path = file.getPath();
		int i = path.lastIndexOf(".");
		if (i < 0) {
			return null;
		} else {
			return path.substring(i + 1);
		}
	}

	@Override
	protected boolean isItemVisible(final File file) {
		/* if the item need to be hidden,
		 *   then we shall not consider it for comparision */
		boolean ret = super.isItemVisible(file);

		if (ret && (m_allowedExt != null) && !isDir(file) &&
			(mode == MODE_FILE || mode == MODE_FILE_AND_DIR)) {
			String ext = getExtension(file);
			for (String e: m_allowedExt) {
				ret = e.equalsIgnoreCase(ext);
				if (!ret) {
					break;
				}
			}
		}

		return ret;
	}
}
