/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.extra.filepicker;

import android.content.Intent;
import android.os.Bundle;

import com.nononsenseapps.filepicker.FilePickerActivity;
import com.nononsenseapps.filepicker.AbstractFilePickerFragment;

import java.io.File;
import java.util.ArrayList;

/**
 * Show files that have the extension that are provided via "ExtFilteredFilePicker.ALLOWED_EXT" extra.
 */
public class ExtFilteredFilePickerActivity extends FilePickerActivity {
	/**
	 * Extra that is passed to enable filtering.
	 *  data can be a single string or array of string
	 */
	public static final String EXTRA_ALLOWED_EXT = "filepicker.intent.ALLOWED_EXT";

	private ArrayList<String> m_allowedExt;

	public ExtFilteredFilePickerActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Intent intent = getIntent();
		if (intent != null) {
			m_allowedExt = getStringArray(intent, EXTRA_ALLOWED_EXT);
		}

		super.onCreate(savedInstanceState);
	}

	protected ExtFilteredFilePickerFragment getFragment(
			final String startPath, final int mode, final boolean allowMultiple,
			final boolean allowCreateDir, final ArrayList<String> allowedExt) {
		ExtFilteredFilePickerFragment fragment = new ExtFilteredFilePickerFragment();
		fragment.setArgs(startPath, mode, allowMultiple, allowCreateDir, allowedExt);
		return fragment;
	}

	@Override
	protected ExtFilteredFilePickerFragment getFragment(
			final String startPath, final int mode, final boolean allowMultiple,
			final boolean allowCreateDir)  {
		Intent intent = getIntent();
		if (intent != null) {
			m_allowedExt = getStringArray(intent, EXTRA_ALLOWED_EXT);
		}

		return getFragment(startPath, mode, allowMultiple, allowCreateDir, m_allowedExt);
	}

	/**
	 * Try to extract a String[] data from intent.
	 * It will accept String, String[] and ArrayList<String>
	 * @param intent Intent to extract from
	 * @param name Name of the data
	 * @return ArrayList<String> if found
	 * @return null if not found
	 */
	public static ArrayList<String> getStringArray(Intent intent, String name) {
		if (!intent.hasExtra(name)) {
			return null;
		}

		/* ArrayList provided */
		ArrayList<String> ret = intent.getStringArrayListExtra(name);
		if (ret != null) {
			return ret;
		}

		/* String[] provided */
		String[] da = intent.getStringArrayExtra(name);
		if (da != null) {
			ret = new ArrayList<String>();
			for (String d: da) {
				ret.add(d);
			}
			return ret;
		}

		/* String provided */
		String d = intent.getStringExtra(name);
		if (d != null) {
			ret = new ArrayList<String>();
			ret.add(d);
			return ret;
		}

		/* nothing provided that can be accepted as ArrayList.
		 *  should not happen */
		return null;
	}
}
