/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.recent.column;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ListView;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.tunnel.ColumnBaseAdapter;

public class ListDialog extends AlertDialog {
	public ListDialog(Context context) {
		super(context);
		if (Column.getColumnCount() > 0) {
			loadListView(context);
		} else {
			loadEmptyView(context);
		}
	}

	private void loadEmptyView(Context context) {
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.large_empty_text, null);
		setView(rootView);
	}

	private void loadListView(Context context) {
		ListView listView = new ListView(context);
		InternalAdapter adapter = new InternalAdapter(context);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(adapter);
		setView(listView);
	}

	private static class InternalAdapter extends ColumnBaseAdapter
				implements AdapterView.OnItemClickListener {
		private final LayoutInflater m_inflater;
		public InternalAdapter(Context context) {
			m_inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = m_inflater.inflate(
					android.R.layout.simple_list_item_multiple_choice , parent, false);
			}

			CheckedTextView checkedTextView = (CheckedTextView) convertView;
			checkedTextView.setText(Column.getName(position));
			checkedTextView.setChecked(Column.isVisible(position));
			checkedTextView.setTextColor(Column.getColor(position));
			return checkedTextView;
		}

		@Override
		public int getCount() {
			return Column.getColumnCount();
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void onItemClick(AdapterView<?> parent, View view,
			int position, long id) {
			CheckedTextView checkedTextView = (CheckedTextView) view;
			checkedTextView.toggle(); /* JUST WORKS! */
			boolean visible = checkedTextView.isChecked();
			Column.setVisible(position, visible);
		}
	}
}
