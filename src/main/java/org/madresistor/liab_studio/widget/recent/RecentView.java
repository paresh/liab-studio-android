/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.recent;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;

/* TODO: do something so that update is only performed when View is visible */

public class RecentView extends TableLayout {
	private LayoutInflater m_inflater;

	public RecentView(Context context) {
		super(context);
		init(context);
	}

	public RecentView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		m_inflater = LayoutInflater.from(context);
	}

	public void update() {
		int rows = 0;
		for (int i = 0; i < Column.getColumnCount(); i++) {
			if (Column.isVisible(i)) {
				View view = getChildAt(rows);
				if (view == null) {
					//~ view = new TableRow();
					view = m_inflater.inflate(
						R.layout.widget_recent_view_row, this, false);
					TagHolder tag = new TagHolder(view);
					addView(view, rows);
					view.setTag(R.id.widget_recent_view_row_holder, tag);
				}

				TagHolder tag = (TagHolder) view.getTag(R.id.widget_recent_view_row_holder);
				tag.name.setText(Column.getName(i));
				tag.value.setText(Column.getRecentValueText(i));
				tag.unit.setText(Column.getUnit(i));

				int color = Column.getColor(i);
				tag.name.setTextColor(color);
				tag.value.setTextColor(color);
				tag.unit.setTextColor(color);

				rows++;
			}
		}

		/* remove extra rows */
		int child_count = getChildCount();
		if (rows < child_count) {
			removeViews(rows, child_count - rows);
		}
	}

	private static class TagHolder {
		public final TextView name, value, unit;
		public TagHolder(View view) {
			name = (TextView) view.findViewById(R.id.widget_recent_view_row_name);
			value = (TextView) view.findViewById(R.id.widget_recent_view_row_value);
			unit = (TextView) view.findViewById(R.id.widget_recent_view_row_unit);
		}
	}
}
