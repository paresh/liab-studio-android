/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.table;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inqbarna.tablefixheaders.TableFixHeaders;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.widget.table.column.EditDialog;

public class NonCellView extends TextView {
	public NonCellView(Context context) {
		super(context);
		init();
	}

	public NonCellView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private static final EditMeOnClick editMeOnClick = new EditMeOnClick();
	private void init() {
		setGravity(Gravity.CENTER);

		setLayoutParams(new ViewGroup.MarginLayoutParams(
			ViewGroup.LayoutParams.WRAP_CONTENT,
			ViewGroup.LayoutParams.WRAP_CONTENT));
		int padding = (int) getResources().getDimension(R.dimen.widget_table_noncell_padding);
		setPadding(padding, padding, padding, padding);
		setBackgroundResource(R.drawable.widget_table_noncell);
	}

	public void update(int row, int col) {
		int color;
		String text;
		if (col == -1) {
			text = (row == -1) ? "" : Integer.toString(row + 1);
			color = R.color.widget_table_number;
		} else {
			text = Column.getName(col);
			color = Column.getColor(col);
		}

		setText(text);
		setTextColor(color);

		setOnClickListener((row == -1 && col >= 0) ? editMeOnClick : null);
	}

	private static class EditMeOnClick implements View.OnClickListener {
		@Override
		public void onClick(View view) {
			int col_num = TableFixHeaders.getColumn(view);
			Context context = view.getContext();
			EditDialog dialog = new EditDialog(context, col_num);
			dialog.show();
		}
	}
}
