/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.table.column;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;

public class ClearAllDialog extends AlertDialog
	implements DialogInterface.OnClickListener {
	public ClearAllDialog(Context context) {
		super(context);

		setIcon(R.drawable.ic_alert_grey600_48dp);
		setTitle(context.getString(R.string.data_loss));
		setMessage(context.getString(R.string.loss_of_all_value));

		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.clear_all), this);

		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.cancel), this);
	}

	public void onClick(DialogInterface dialog, int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			Column.clearAllColumn();
		}

		dismiss();
	}
}
