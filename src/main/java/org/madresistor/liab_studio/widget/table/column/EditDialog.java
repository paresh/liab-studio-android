/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.table.column;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.ui.EditBackgroundColorOnClick;
import org.madresistor.liab_studio.widget.table.column.EditMoreDialog;

public class EditDialog extends AlertDialog
	implements DialogInterface.OnClickListener,
				View.OnClickListener {
	private static final String TAG = EditDialog.class.getName();
	private final int m_column;

	EditText m_name;
	EditText m_unit;
	EditText m_lineWidth;
	Button m_color;
	Spinner m_sigDigits;
	SwitchCompat m_editable;

	/*
	 * Ref: http://code.google.com/p/android/issues/detail?id=19360
	 */
	public EditDialog(Context context, int column) {
		super(context);
		m_column = column;

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.widget_table_column_options, null);
		setView(rootView);

		/* inputs */
		m_name = (EditText) rootView.findViewById(
			R.id.widget_table_column_options_name);
		m_unit = (EditText) rootView.findViewById(
			R.id.widget_table_column_options_unit);
		m_lineWidth = (EditText) rootView.findViewById(
			R.id.widget_table_column_options_line_width);
		m_color = (Button) rootView.findViewById(
			R.id.widget_table_column_options_color);
		m_sigDigits = (Spinner) rootView.findViewById(
			R.id.widget_table_column_options_sig_digits);
		m_editable = (SwitchCompat) rootView.findViewById(
			R.id.widget_table_column_options_editable);
		m_color.setOnClickListener(
			EditBackgroundColorOnClick.getInstance());

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.done), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.cancel), this);

		/* negative button */
		setButton(DialogInterface.BUTTON_NEGATIVE,
			context.getString(R.string.more), this);

		/* title */
		setTitle(context.getString(R.string.modify_column));

		/* icon */
		setIcon(R.drawable.ic_pencil_grey600_48dp);

		/* set values */
		m_name.setText(Column.getName(column));
		m_unit.setText(Column.getUnit(column));
		m_lineWidth.setText(Integer.toString(Column.getLineWidth(column)));
		m_color.setBackgroundColor(Column.getColor(column));
		m_sigDigits.setSelection(Column.getSigDigits(column) + 1);
		m_editable.setChecked(Column.isEditable(column));
	}

	@Override
	public void show() {
		super.show();

		/* prevent auto closing of "More" */
		getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(this);
	}

	public void onClick(DialogInterface dialog, int which) {
		switch(which) {
		case DialogInterface.BUTTON_POSITIVE:
			int column = m_column;
			Column.setName(column, m_name.getText().toString());
			Column.setUnit(column, m_unit.getText().toString());

			try {
				int line_width =
					Integer.parseInt(m_lineWidth.getText().toString());
				Column.setLineWidth(column, line_width);
			} catch(NumberFormatException e) {
				if (Config.WTF) Log.wtf(TAG, "parse failed for line_width");
				e.printStackTrace();
			}

			ColorDrawable drawable = (ColorDrawable) m_color.getBackground();
			Column.setColor(column, drawable.getColor());
			Column.setSigDigits(column,
				m_sigDigits.getSelectedItemPosition() - 1);
			Column.setEditable(column, m_editable.isChecked());
		break;
		case DialogInterface.BUTTON_NEUTRAL:
			dismiss();
		break;
		case DialogInterface.BUTTON_NEGATIVE:
			/* never called, see Edit.onClick(View) */
		break;
		}
	}

	public int getColumn() {
		return m_column;
	}

	public void onClick(View view) {
		EditMoreDialog more = new EditMoreDialog(this);
		more.show();
	}
}
