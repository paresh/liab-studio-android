/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.table;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.widget.table.CellView;
import org.madresistor.liab_studio.widget.table.NonCellView;

public class TableAdapter extends BaseTableAdapter
	implements Column.Observer {
	private final Context m_context;

	public TableAdapter(Context context) {
		m_context = context;

		Resources r = context.getResources();
		m_width_cell = Math.round(TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP,
			WIDTH_DIP, r.getDisplayMetrics()));
		m_height = Math.round(TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP,
			HEIGHT_DIP, r.getDisplayMetrics()));

		Column.registerObserver(this);
	}

	@Override
	public View getView(int row, int col, View convertView, ViewGroup parent) {
		if (convertView == null) {
			if (row >= 0 && col >= 0) {
				convertView = new CellView(m_context);
			} else {
				convertView = new NonCellView(m_context);
			}
		}

		if (row >= 0 && col >= 0) {
			CellView cellView = (CellView) convertView;
			cellView.update(row, col);
		} else {
			NonCellView nonCellView = (NonCellView) convertView;
			nonCellView.update(row, col);
		}

		convertView.clearFocus();
		return convertView;
	}

	/*
	 * Type can only be shared when
	 *  row == -1 || column == -1, NonCellView
	 *  row >= 0, column >= 0, CellView
	 */

	@Override
	public int getItemViewType(int row, int col) {
		return (row >= 0 && col >= 0) ? 0 : 1;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	private final static int WIDTH_DIP = 128;
	private final static int HEIGHT_DIP = 48;
	private final int m_width_cell, m_height;

	@Override
	public int getHeight(int row) {
		return m_height;
	}

	@Override
	public int getWidth(int col) {
		if (col == -1) {
			Resources r = m_context.getResources();
			return Math.round(TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP,
				48, r.getDisplayMetrics()));
		}

		return m_width_cell;
	}

	@Override
	public int getRowCount() {
		return Column.getRowCount() + 100;
	}

	@Override
	public int getColumnCount() {
		return Column.getColumnCount();
	}

	@Override
	public void onColumnChanged() {
		notifyDataSetChanged();
	}
}

