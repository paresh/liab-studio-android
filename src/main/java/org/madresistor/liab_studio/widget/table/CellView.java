/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.table;

import android.content.Context;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.inqbarna.tablefixheaders.TableFixHeaders;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.extra.ScientificNotationKeyListener;
import org.madresistor.liab_studio.tunnel.Column;

public class CellView extends EditText {
	public CellView(Context context) {
		super(context);
		init();
	}

	public CellView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private boolean m_editable = false;
	private static final GotoStartOnFocus gotoStartOnFocus = new GotoStartOnFocus();
	private static final GotoStartOnFirstTouch gotoStartOnFirstTouch = new GotoStartOnFirstTouch();
	private        final CellKeyListener cellKeyListener = new CellKeyListener();
	private        final Sync sync = new Sync();

	private void init() {
		setLayoutParams(new ViewGroup.MarginLayoutParams(
			ViewGroup.LayoutParams.WRAP_CONTENT,
			ViewGroup.LayoutParams.WRAP_CONTENT));
		addTextChangedListener(sync);
		setKeyListener(cellKeyListener);
		setOnFocusChangeListener(gotoStartOnFocus);
		setOnTouchListener(gotoStartOnFirstTouch);
		setEllipsize(TextUtils.TruncateAt.END);
		setSingleLine();
		setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
		setBackgroundResource(R.drawable.widget_table_cell);
		int padding = (int) getResources().getDimension(R.dimen.widget_table_cell_padding);
		setPadding(padding, padding, padding, padding);
	}

	public void update(int row, int col) {
		m_editable = false;
		setText(Column.getValueText(row, col));

		m_editable = Column.isEditable(col);
		setTextColor(Column.getColor(col));
	}

	/*
	 * Ref: http://stackoverflow.com/a/4283532/1500988
	 */
	private class Sync implements TextWatcher {
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if (m_editable) {
				try {
					int row = TableFixHeaders.getRow(CellView.this);
					int col = TableFixHeaders.getColumn(CellView.this);
					Log.d("LiaBStudio/CellView", "row=" + row + ", col=" + col);
					Column.setValueText(row, col, s.toString());
				} catch(IllegalArgumentException e) {
					//e.printStackTrace();
				}
			}
		}

		public void afterTextChanged(Editable s) {

		}
	}

	private static class GotoStartOnFocus implements View.OnFocusChangeListener {
		public void onFocusChange(View v, boolean hasFocus) {
			if (hasFocus) {
				CellView cellView = (CellView) v;
				int position = cellView.getText().length();
				cellView.setSelection(position);
			}
		}
	}

	private static class GotoStartOnFirstTouch implements View.OnTouchListener {
		public boolean onTouch(View v, MotionEvent event) {
			/*
			 * Trick
			 *  if user is selecting the EditText for first time.
			 *    goto the end
			 */
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				CellView cellView = (CellView) v;
				if (!cellView.hasFocus()) {
					int position = cellView.getText().length();
					cellView.setSelection(position);
				}
			}

			/* dont consume or EditText wont get focused */
			return false;
		}
	}

	private static final String EMPTY = "";

	private class CellKeyListener extends ScientificNotationKeyListener {
		@Override
		public CharSequence filter(CharSequence source, int start, int end,
							   Spanned dest, int dstart, int dend) {
			if (m_editable) {
				return super.filter(source, start, end, dest, dstart, dend);
			}

			return EMPTY;
		}
	}
}
