/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.table.column;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.Extra;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.ui.EditBackgroundColorOnClick;

/* in Add dialog, hiding editable field because it is useless to ask user this field.
 * in order to make the new field useful, user will enter data, but it disable editing,
 * then their is no way of entering data.
 * making the input editable meaning less
 */

public class AddDialog extends AlertDialog
	implements DialogInterface.OnClickListener {
	private static final String TAG = AddDialog.class.getName();

	EditText m_name;
	EditText m_unit;
	EditText m_lineWidth;
	Button m_color;
	Spinner m_sigDigits;

	/*
	 * Ref: http://code.google.com/p/android/issues/detail?id=19360
	 */
	public AddDialog(Context context) {
		super(context);

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.widget_table_column_options, null);
		setView(rootView);

		/* inputs */
		m_name = (EditText) rootView.findViewById(
			R.id.widget_table_column_options_name);
		m_unit = (EditText) rootView.findViewById(
			R.id.widget_table_column_options_unit);
		m_lineWidth = (EditText) rootView.findViewById(
			R.id.widget_table_column_options_line_width);
		m_color = (Button) rootView.findViewById(
			R.id.widget_table_column_options_color);
		m_sigDigits = (Spinner) rootView.findViewById(
			R.id.widget_table_column_options_sig_digits);

		/* hide editable switch */
		rootView
			.findViewById(R.id.widget_table_column_options_editable)
			.setVisibility(View.GONE);

		m_color.setOnClickListener(
			EditBackgroundColorOnClick.getInstance());

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.add), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.discard), this);

		/* title */
		setTitle(context.getString(R.string.add_column));

		/* icon */
		setIcon(R.drawable.ic_action_column_add);

		m_name.setText("Column");
		m_lineWidth.setText("1");
		m_color.setBackgroundColor(Extra.newColorForUser());
		m_sigDigits.setSelection(0);
	}

	public void onClick(DialogInterface dialog, int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			int column = Column.appendColumn();
			Column.setName(column, m_name.getText().toString());
			Column.setUnit(column, m_unit.getText().toString());

			try {
				int line_width =
					Integer.parseInt(m_lineWidth.getText().toString());
				Column.setLineWidth(column, line_width);
			} catch(NumberFormatException e) {
				if (Config.WTF) Log.wtf(TAG, "parse failed for line_width");
				e.printStackTrace();
			}

			ColorDrawable drawable = (ColorDrawable) m_color.getBackground();
			Column.setColor(column, drawable.getColor());
			Column.setSigDigits(column,
				m_sigDigits.getSelectedItemPosition() - 1);
			Column.setEditable(column, true);
		}
	}
}
