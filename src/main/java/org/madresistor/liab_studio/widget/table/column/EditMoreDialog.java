/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.table.column;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.widget.table.column.EditDialog;

public class EditMoreDialog extends AlertDialog
	implements DialogInterface.OnClickListener,
				View.OnClickListener {
	private final EditDialog m_parent;
	private final View m_remove;
	private final View m_clear;

	public EditMoreDialog(EditDialog parent) {
		super(parent.getContext());
		Context context = parent.getContext();
		m_parent = parent;

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.widget_table_column_edit_more, null);
		setView(rootView);

		m_remove = rootView.findViewById(
			R.id.widget_table_column_edit_more_remove);
		m_remove.setOnClickListener(this);

		m_clear = rootView.findViewById(
			R.id.widget_table_column_edit_more_clear);
		m_clear.setOnClickListener(this);

		setTitle(context.getString(R.string.data_loss));
		setIcon(R.drawable.ic_alert_grey600_48dp);

		/* positive button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.back), this);
	}

	public void onClick(View view) {
		int column = m_parent.getColumn();
		if (view == m_remove) {
			Column.removeColumn(column);
			m_parent.dismiss();
		} else if (view == m_clear) {
			Column.clearColumn(column);
		}

		dismiss();
	}

	public void onClick(DialogInterface dialog, int which) {
		dismiss();
	}
}
