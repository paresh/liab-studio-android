/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.table.csv;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;

import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.extra.OperationThread;

/**
 * Thread to import Column data to CSV file.
 *  No need to call start() as it is already done by the constructor.
 */
public class Importer extends OperationThread {
	private final Context m_context;
	private final File m_file;

	public Importer(Context context, File file) {
		super("CSV Importer");
		m_context = context;
		m_file = file;
		start();
	}

	@Override
	public void run() {
		try {
			FileInputStream fis = new FileInputStream(m_file);
			Column.importCsv(fis.getFD());
			fis.close();
		} catch(Exception e) {
			e.printStackTrace();
			failedDialog(m_context, e);
		}
	}
 }
