/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.graph.curve;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.tunnel.Curve;
import org.madresistor.liab_studio.tunnel.CurveBaseAdapter;
import org.madresistor.liab_studio.ui.EditBackgroundColorOnClick;

public class AddDialog extends AlertDialog
	implements DialogInterface.OnClickListener,
				AdapterView.OnItemSelectedListener {
	private static final String TAG = AddDialog.class.getName();
	private final Context m_context;

	Spinner m_x, m_y;
	EditText m_name;
	EditText m_lineWidth;
	Button m_color;
	String m_name_format;

	/**
	 * @param context Context of the activity that created this.
	 * Ref: http://code.google.com/p/android/issues/detail?id=19360
	 */
	public AddDialog(Context context) {
		super(context);
		m_context = context;

		m_name_format = m_context.getString(R.string.widget_curve_name_format);

		/* TODO: test using Column.getColumnCount() if their is any column.
		 *   if equals 0, show that no column exists */

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.widget_graph_curve_options, null);
		setView(rootView);

		/* inputs */
		m_x = (Spinner) rootView.findViewById(
			R.id.widget_graph_curve_options_x);
		m_y = (Spinner) rootView.findViewById(
			R.id.widget_graph_curve_options_y);
		m_name = (EditText) rootView.findViewById(
			R.id.widget_graph_curve_options_name);
		m_lineWidth = (EditText) rootView.findViewById(
			R.id.widget_graph_curve_options_line_width);
		m_color = (Button) rootView.findViewById(
			R.id.widget_graph_curve_options_color);
		m_color.setOnClickListener(
			EditBackgroundColorOnClick.getInstance());

		/* adapters */
		m_x.setAdapter(new ColumnPopulateAdapter(context));
		m_y.setAdapter(new ColumnPopulateAdapter(context));

		int res_id;

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.add), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.discard), this);

		/* title */
		setTitle(context.getString(R.string.add_curve));

		/* title */
		setIcon(R.drawable.ic_action_curve_add);

		if (Column.getColumnCount() > 0) {
			m_x.setSelection(0);
		}

		/* column pre selected (for x its default 0) */
		if (Column.getColumnCount() > 1) {
			m_y.setSelection(1);
		}

		/* update inputs so that user dont have to input defaults */
		updateInputs();

		/* listener, when user is adding a new curve */
		m_x.setOnItemSelectedListener(this);
		m_y.setOnItemSelectedListener(this);
	}

	public void onClick(DialogInterface dialog, int which) {
		switch(which) {
		case DialogInterface.BUTTON_POSITIVE:
			int x = m_x.getSelectedItemPosition();
			int y = m_y.getSelectedItemPosition();
			int curve = Curve.appendCurve(x, y);

			Curve.setName(curve, m_name.getText().toString());

			try {
				int line_width =
					Integer.parseInt(m_lineWidth.getText().toString());
				Curve.setLineWidth(curve, line_width);
			} catch(NumberFormatException e) {
				if (Config.WTF) Log.wtf(TAG, "parse failed for line_width");
				e.printStackTrace();
			}

			ColorDrawable drawable = (ColorDrawable) m_color.getBackground();
			Curve.setColor(curve, drawable.getColor());
		break;
		case DialogInterface.BUTTON_NEUTRAL:
			cancel();
		break;
		}
	}

	private static class ColumnPopulateAdapter extends CurveBaseAdapter {
		private final LayoutInflater m_inflater;
		public ColumnPopulateAdapter(Context context) {
			m_inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position,
			View convertView, ViewGroup parent) {
			return getView(android.R.layout.simple_spinner_item,
						position, convertView, parent);
		}

		@Override
		public View getDropDownView(int position,
			View convertView, ViewGroup parent) {
			return getView(android.R.layout.simple_spinner_dropdown_item,
						position, convertView, parent);
		}

		/* generic method to get a view from column and layout type */
		private View getView(int layout_res, int position,
			View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = m_inflater.inflate(layout_res , parent, false);
			}

			TextView textView = (TextView)
				convertView.findViewById(android.R.id.text1);
			textView.setText(Column.getName(position));
			textView.setTextColor(Column.getColor(position));
			return textView;
		}

		@Override
		public int getCount() {
			return Column.getColumnCount();
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
	}

	private void updateInputs() {
		int colX = m_x.getSelectedItemPosition();
		int colY = m_y.getSelectedItemPosition();
		if (Config.DEBUG) Log.d(TAG, "updateInputs: x = " + colX + ", y = " + colY);

		/* build up a string for name */
		String nameX = Column.getName(colX);
		String nameY = Column.getName(colY);

		String name = String.format(m_name_format, nameY, nameX);

		int color = Column.getColor(colY);
		int lineWidth = Column.getLineWidth(colY);

		m_name.setText(name);
		m_color.setBackgroundColor(color);
		m_lineWidth.setText(Integer.toString(lineWidth));
	}

	/* update Name, LineWidth, Color */
	public void onItemSelected(AdapterView<?> parent,
		View view, int position, long id) {
		updateInputs();
	}

	public void onNothingSelected(AdapterView<?> parent) {
		/* do nothing */
	}
}
