/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.graph;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Curve;
import org.madresistor.liab_studio.widget.Widget;
import org.madresistor.liab_studio.widget.graph.PlotView;
import org.madresistor.liab_studio.widget.graph.curve.AddDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Graph extends Widget implements Curve.Observer {
	private final static String TAG = Graph.class.getName();

	public Graph(Context context) {
		super(context);
		Curve.registerObserver(this);
	}

	private final Frontend m_frontend = new Frontend();
	public Frontend getFrontend() {
		return m_frontend;
	}

	/**
	 * Capture the PlotView and store the capture as PNG.
	 * PNG was consider because it is Lossless
	 */
	private void exportPng() {
		File file = new File(getPicturesDirectory(), getFileNameUsingTitle() + ".png");
		new PngExporter(m_frontend.m_plot, file);
	}

	public void onCurveChanged() {
		m_frontend.update();
	}

	private class Frontend extends Widget.Frontend {
		private PlotView m_plot = null;

		public Frontend() {
			/* Fragment participate in menu creation */
			setHasOptionsMenu(true);

			/* Retain state */
			setRetainInstance(true);
		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.widget_graph, container, false);
			m_plot = (PlotView) rootView.findViewById(R.id.widget_graph_plot);
			return rootView;
		}

		@Override
		public void onDestroyView() {
			m_plot = null;
			super.onDestroyView();
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.widget_graph_actions, menu);
		}

		public void update() {
			if (m_plot != null) {
				m_plot.requestRender();
			}
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch(item.getItemId()) {
			case R.id.widget_graph_export: {
				exportPng();
			} return true;
			case R.id.widget_graph_list: {
				ListDialog dialog = new ListDialog(getActivity());
				dialog.show();
			} return true;
			case R.id.widget_graph_add: {
				AddDialog dialog = new AddDialog(getActivity());
				dialog.show();
			} return true;
			default:
			return super.onOptionsItemSelected(item);
			}
		}
	}

	static {
		Widget.register(Graph.class);
	}
}
