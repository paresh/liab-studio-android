/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget.graph.curve;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.madresistor.liab_studio.Config;
import org.madresistor.liab_studio.R;
import org.madresistor.liab_studio.tunnel.Column;
import org.madresistor.liab_studio.tunnel.Curve;
import org.madresistor.liab_studio.tunnel.CurveBaseAdapter;
import org.madresistor.liab_studio.ui.EditBackgroundColorOnClick;

public class EditDialog extends AlertDialog
	implements DialogInterface.OnClickListener {
	private static final String TAG = EditDialog.class.getName();
	private final int m_curve;
	private final Context m_context;

	Spinner m_x, m_y;
	EditText m_name;
	EditText m_lineWidth;
	Button m_color;

	/*
	 * Ref: http://code.google.com/p/android/issues/detail?id=19360
	 */
	public EditDialog(Context context, int curve) {
		super(context);
		m_curve = curve;
		m_context = context;

		/* content */
		LayoutInflater infater = LayoutInflater.from(context);
		View rootView = infater.inflate(
			R.layout.widget_graph_curve_options, null);
		setView(rootView);

		/* inputs */
		m_x = (Spinner) rootView.findViewById(
			R.id.widget_graph_curve_options_x);
		m_y = (Spinner) rootView.findViewById(
			R.id.widget_graph_curve_options_y);
		m_name = (EditText) rootView.findViewById(
			R.id.widget_graph_curve_options_name);
		m_lineWidth = (EditText) rootView.findViewById(
			R.id.widget_graph_curve_options_line_width);
		m_color = (Button) rootView.findViewById(
			R.id.widget_graph_curve_options_color);
		m_color.setOnClickListener(
			EditBackgroundColorOnClick.getInstance());

		/* adapters */
		m_x.setAdapter(new ColumnPopulateAdapter(context));
		m_y.setAdapter(new ColumnPopulateAdapter(context));

		int res_id;

		/* positive button */
		setButton(DialogInterface.BUTTON_POSITIVE,
			context.getString(R.string.done), this);

		/* neutral button */
		setButton(DialogInterface.BUTTON_NEUTRAL,
			context.getString(R.string.back), this);

		/* negative button */
		setButton(DialogInterface.BUTTON_NEGATIVE,
			context.getString(R.string.delete), this);

		/* title */
		setTitle(context.getString(R.string.modify_curve));

		/* title */
		setIcon(R.drawable.ic_pencil_grey600_48dp);

		/* set values */
		m_x.setSelection(Curve.getColumnX(curve));
		m_y.setSelection(Curve.getColumnY(curve));
		m_name.setText(Curve.getName(curve));
		m_lineWidth.setText(Float.toString(Curve.getLineWidth(curve)));
		m_color.setBackgroundColor(Curve.getColor(curve));
	}

	public void onClick(DialogInterface dialog, int which) {
		switch(which) {
		case DialogInterface.BUTTON_POSITIVE:
			int curve = m_curve;
			Curve.setName(curve, m_name.getText().toString());

			try {
				float line_width = Float.parseFloat(m_lineWidth.getText().toString());
				Curve.setLineWidth(curve, line_width);
			} catch(NumberFormatException e) {
				if (Config.WTF) Log.wtf(TAG, "parse failed for line_width");
				e.printStackTrace();
			}

			ColorDrawable drawable = (ColorDrawable) m_color.getBackground();
			Curve.setColor(curve, drawable.getColor());
		break;
		case DialogInterface.BUTTON_NEUTRAL:
			dismiss();
		break;
		case DialogInterface.BUTTON_NEGATIVE:
			Curve.removeCurve(m_curve);
		break;
		}
	}

	private static class ColumnPopulateAdapter extends CurveBaseAdapter {
		private final LayoutInflater m_inflater;
		public ColumnPopulateAdapter(Context context) {
			m_inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return getView(android.R.layout.simple_spinner_item,
						position, convertView, parent);
		}

		@Override
		public View getDropDownView(int position, View convertView, ViewGroup parent) {
			return getView(android.R.layout.simple_spinner_dropdown_item,
						position, convertView, parent);
		}

		/* generic method to get a view from column and layout type */
		private View getView(int layout_res, int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = m_inflater.inflate(layout_res , parent, false);
			}

			TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
			textView.setText(Column.getName(position));
			textView.setTextColor(Column.getColor(position));
			return textView;
		}

		@Override
		public int getCount() {
			return Column.getColumnCount();
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
	}
}
