/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.widget;

import android.content.Context;

import org.madresistor.liab_studio.JniLoader;
import org.madresistor.liab_studio.Module;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A widget is user interface that shows data to user. (Consumer Module)
 */
public abstract class Widget extends Module {
	/**
	 * Construct a Widget
	 * @param context Context of the activity that created it
	 */
	public Widget(Context context) {
		super(context);
	}

	/** title of the project (user is working on) */
	private String m_title = null;

	/**
	 * Get title of module.
	 * default implementation return the text that
	 *  has been passed to onTitleChanged() or null
	 * @return title
	 */
	public String getTitle() {
		return m_title;
	}

	/**
	 * generate a file name using title.
	 * if the user have not given any, then auto generate a one using Timestamp.
	 * @return a non null string.
	 */
	public String getFileNameUsingTitle() {
		String title = getTitle();
		if (title == null) {
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateTime = fmt.format(Calendar.getInstance().getTime());
			title = "experiment " + dateTime;
		}

		return title;
	}

	/**
	 * This method set the current title that is user working on.
	 * store the title for later reterival via getTitle()
	 * @param title Title
	 */
	void onTitleChanged(String title) {
		m_title = title;
	}

	/**
	 * This is the frontend that is for a module.
	 * currently extends Module.Frontend but maybe we can find use in future.
	 * All module need to override this frontend
	 */
	public abstract class Frontend extends Module.Frontend {

	}

	public final static List< Class<Widget> > registered = new ArrayList< Class<Widget> >();
	protected static void register(Class<? extends Widget> widget) {
		registered.add((Class<Widget>) widget);
	}

	static {
		JniLoader.glue();
	}
}
