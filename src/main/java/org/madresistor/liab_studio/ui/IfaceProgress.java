/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio.ui;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

import org.madresistor.liab_studio.iface.Iface;

public class IfaceProgress extends ProgressBar implements Iface.OnStatusChangedListener {
	public IfaceProgress(Context context) {
		super(context);
	}

	public IfaceProgress(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/* as per calc.
	 *  ((99 * 60 * 60) + (99 * 60) + 99) * 1000 require ~28.4bits,
	 *   so no overflow expected */

	private Updater active_updater;
	public void onStatusChanged(Iface iface, int value)  {
		/* remove any previous updater */
		if (active_updater != null) {
			active_updater.cancel(true);
			active_updater = null;
		}

		if (value == Iface.OnStatusChangedListener.STOP) {
			setVisibility(View.GONE);
		} else if (value == Iface.OnStatusChangedListener.INFINITE) {
			setVisibility(View.VISIBLE);
			setIndeterminate(true);
		} else {
			active_updater = new Updater();
			active_updater.execute(value);
		}
	}

	private final static long SLEEP_DURATION_MS = 100;
	private class Updater extends AsyncTask<Integer, Integer, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			setVisibility(View.VISIBLE);
			setIndeterminate(false);
		}

		@Override
		protected Void doInBackground(Integer... params) {
			long millis = params[0] * 1000;
			setMax((int) millis);
			setProgress(0);

			long diff, start = SystemClock.elapsedRealtime();
			do {
				if (isCancelled()) {
					break;
				}

				/* sleep for some time */
				try {
					Thread.sleep(SLEEP_DURATION_MS);
				} catch (InterruptedException e) {
					e.printStackTrace();
					break;
				}

				/* get the number of millis spent from start */
				diff = SystemClock.elapsedRealtime() - start;
				publishProgress((int) diff);
			} while (diff < millis);

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			setProgress(values[0]);
		}

		@Override
		protected void onPostExecute(Void results) {
			super.onPostExecute(results);
			setVisibility(View.GONE);
			active_updater = null;
		}
	}
}
