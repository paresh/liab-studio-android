/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio;

import android.util.Log;

import org.madresistor.box0.Device;

public class Config {
	/* some handy constants to change for production */
	public static final boolean DEVELOPER_MODE = true;
	public static final Device.Log BOX0_LOG_LEVEL = Device.Log.DEBUG;
	public static final int LOG_LEVEL = Log.VERBOSE;

	/* NOTE: compile libusb and libbox0 with default log_level as per above requirement */
	public static final boolean WARN = LOG_LEVEL <= Log.WARN;
	public static final boolean VERBOSE = LOG_LEVEL <= Log.VERBOSE;
	public static final boolean DEBUG = LOG_LEVEL <= Log.DEBUG;
	public static final boolean WTF = true;
	public static final boolean INFO = LOG_LEVEL <= Log.INFO;
	public static final boolean ERROR = LOG_LEVEL <= Log.ERROR;
	public static final boolean ASSERT = LOG_LEVEL <= Log.ASSERT;
}
