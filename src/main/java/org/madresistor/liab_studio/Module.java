/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.madresistor.liab_studio;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Build;
import android.os.Environment;

public abstract class Module {
	private final Context m_context;

	public Module(Context context) {
		/* derived class do init work here */
		m_context = context;
	}

	/**
	 * Get context of the activity that created the module.
	 * @return context
	 */
	public Context getContext() {
		return m_context;
	}

	/**
	 * The subclass should override this method to
	 *  provide a Frontend that the will be displayed to user.
	 * @return fronend
	 */
	abstract public Frontend getFrontend();

	/**
	 * The user interface that is shown to user.
	 */
	public abstract class Frontend extends Fragment {

	}

	/**
	 * The indent that the Activity received are routed to all modules.
	 * @param indent Indent to consume.
	 * @return true if the module has used the indent and user will most likely
	 *  be interested in viewing the module frontend (to see changes made by intent).
	 *  Thought it can never be assured that returning true, will indeed show the frontned
	 */
	public boolean consumeIndent(Intent intent) {
		return false;
	}

	/**
	 * Get the user documents directory.
	 * This will also ensure that the directory exists.
	 * @return The document directory File
	 */
	public static File getDocumentsDirectory() {
		File file;

		if (Build.VERSION.SDK_INT >= 19) {
			String name = android.os.Environment.DIRECTORY_DOCUMENTS;
			file = Environment.getExternalStoragePublicDirectory(name);
		} else {
			file = new File(Environment.getExternalStorageDirectory() + "/Documents");
		}

		file.mkdirs();

		return file;
	}

	/**
	 * Get the user pictures directory.
	 * This will also ensure that the directory exists.
	 * @return The document directory File
	 */
	public static File getPicturesDirectory() {
		File file;

		if (Build.VERSION.SDK_INT >= 19) {
			String name = android.os.Environment.DIRECTORY_PICTURES;
			file = Environment.getExternalStoragePublicDirectory(name);
		} else {
			file = new File(Environment.getExternalStorageDirectory() + "/Pictures");
		}

		file.mkdirs();

		return file;
	}
}
