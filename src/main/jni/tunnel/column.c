/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "org_madresistor_liab_studio_tunnel_Column.h"
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "column.h"
#include "curve.h"
#include <pthread.h>
#include <errno.h>
#include <csv.h>

#define SIG_DIGITS_AUTOMATIC org_madresistor_liab_studio_tunnel_Column_SIG_DIGITS_AUTOMATIC

#if defined(__FAST_MATH__)
# error "do not compile with fast math, "
		"this cause problem in program, "
		"reason: isnan() will always return false"
#endif

void column_changed(JNIEnv *env);

struct ls_column_list native_columns = {
	.lock = PTHREAD_MUTEX_INITIALIZER,
	.list = NULL
};

ssize_t column_get_index_of(ls_column *col)
{
	size_t off = 0;
	ls_column *i = native_columns.list;
	while (i != NULL) {
		if (i == col) {
			return off;
		}

		off++;
		i = i->next;
	}

	return -1;
}

ls_column *column_get_at(size_t c)
{
	size_t bkp = c;
	ls_column *i = native_columns.list;
	while (i != NULL) {
		if (c == 0) {
			break;
		}

		c--;
		i = i->next;
	}

	if (i == NULL) {
		LOG_WARN("no such column: %zu", bkp);
	}

	return i;
}

static void column_defaults_set(ls_column *col)
{
	col->name = NULL;
	col->unit = NULL;
	col->sig_digits = SIG_DIGITS_AUTOMATIC;
	col->color = 0xFF000000;
	col->line_width = 1;
	col->editable = true;
	col->visible = true;
	col->values_len = 0;
	col->values = NULL;
	col->next = NULL;
}

static float* increase_size_to(ls_column *col, size_t new_size)
{
	float *values = realloc(col->values, sizeof(float) * new_size);
	if (values == NULL) {
		LOG_ERROR("realloc returned NULL");
		return NULL;
	}

	if (new_size > col->values_len) {
		size_t i;
		for (i = col->values_len; i < new_size; i++) {
			values[i] = NAN;
		}
	}

	col->values = values;
	col->values_len = new_size;
	return values;
}

static void textify_value(int sig_digits,
	float value, char *buf, size_t buf_len)
{
	if (isnan(value)) {
		buf[0] = '\0';
		return;
	}

	if (sig_digits < 0) {
		snprintf(buf, buf_len, "%.7g", value);
	} else {
		snprintf(buf, buf_len, "%#.*g", sig_digits, value);
	}
}

static jstring textify_value_to_jstring(JNIEnv *env,
	int sig_digits, float value)
{
	char buf[20];
	textify_value(sig_digits, value, buf, sizeof(buf));
	return (*env)->NewStringUTF(env, buf);
}

static void column_values_remove(ls_column *col)
{
	if (col->values != NULL) {
		free(col->values);
	}

	col->values = NULL;
	col->values_len = 0;
}

static void free_column_internal(ls_column *col)
{
	if (col->name != NULL) {
		free(col->name);
		col->name = NULL;
	}

	if (col->unit != NULL) {
		free(col->name);
		col->unit = NULL;
	}

	if (col->values != NULL) {
		free(col->values);
		col->values = NULL;
		col->values_len = 0;
	}
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getColumnCount
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getColumnCount
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	size_t count = 0;
	ls_column *i;
	for (i = native_columns.list; i != NULL; i = i->next) {
		count++;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return count;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getRowCount
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getRowCount
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	size_t value = 0;
	ls_column *i;

	for (i = native_columns.list; i != NULL; i = i->next) {
		size_t tmp = i->values_len;
		if (tmp > value) {
			value = tmp;
		}
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return value;
}

/**
 * Clear all column.
 * @param env JNI Enviroment (can be NULL)
 * @note if @a env is NULL, callback to Java is not performed
 */
static void column_remove_all()
{
	curve_remove_all();

	ls_column *i = native_columns.list;
	native_columns.list = NULL;
	while (i != NULL) {
		ls_column *tmp = i;
		i = i->next;

		free_column_internal(tmp);
		free(tmp);
	}
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    removeAllColumn
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_removeAllColumn
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	column_remove_all();

	RELEASE_NATIVE_COLUMNS_LOCK

	curve_changed(env);
	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    clearAllColumn
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_clearAllColumn
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *i;
	for (i = native_columns.list; i != NULL; i = i->next) {
		column_values_remove(i);
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    appendColumn
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_appendColumn
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);

	size_t off = 0;
	ls_column *entry = malloc(sizeof(*entry));
	if (entry == NULL) {
		THROW_RUNTIME_EXCEPTION(env, "malloc returned NULL")
		return -1;
	}

	column_defaults_set(entry);

	HOLD_NATIVE_COLUMNS_LOCK

	if (native_columns.list == NULL) {
		native_columns.list = entry;
	} else {
		ls_column *i = native_columns.list;
		while (i->next != NULL) {
			off++;
			i = i->next;
		}

		off++;
		i->next = entry;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);

	return (jint) off;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    removeColumn
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_removeColumn
  (JNIEnv *env, jclass cls, jint c)
{
	HOLD_NATIVE_COLUMNS_LOCK

	size_t off = 0;
	ls_column *i = native_columns.list, *prev = NULL;
	while (i != NULL) {
		if (off++ == c) {
			break;
		}

		prev = i;
		i = i->next;
	}

	if (i == NULL) {
		LOG_WARN("column (%i) do not exists", c);
		goto done;
	}

	if (prev == NULL) {
		native_columns.list = i->next;
	} else {
		prev->next = i->next;
	}

	HOLD_NATIVE_CURVES_LOCK
	curve_column_about_to_be_remove(i);
	RELEASE_NATIVE_CURVES_LOCK

	free_column_internal(i);
	free(i);

	/* FIX: free'g but i maybe be in use. chances: rare(ie none) */

	done:
	RELEASE_NATIVE_COLUMNS_LOCK

	curve_changed(env);
	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    clearColumn
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_clearColumn
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		column_values_remove(col);
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getValue
 * Signature: (II)F
 */
JNIEXPORT jfloat JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getValue
  (JNIEnv *env, jclass cls, jint r, jint c)
{
	UNUSED(cls);

	float value = NAN;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		if (r < col->values_len) {
			value = col->values[r];
		}
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return value;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setValue
 * Signature: (IIF)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setValue
  (JNIEnv *env, jclass cls, jint r, jint c, jfloat value)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col == NULL) {
		goto done;
	}

	if (!col->editable) {
		LOG_WARN("trying to set value to uneditable column");
		goto done;
	}

	if (col->values_len <= r) {
		if (isnan(value)) {
			/* dont write values that are NAN and
			 * require to increase the array
			 */
			goto done;
		}

		if (increase_size_to(col, r + 1) == NULL) {
			THROW_RUNTIME_EXCEPTION(env, "Unable to increase length of array");
			goto done;
		}
	}

	col->values[r] = value;

	done:
	RELEASE_NATIVE_COLUMNS_LOCK
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getValueText
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getValueText
  (JNIEnv *env, jclass cls, jint r, jint c)
{
	UNUSED(cls);

	int sig_digits = -1;
	float value = NAN;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		if (r < col->values_len) {
			value = col->values[r];
		}

		sig_digits = col->sig_digits;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	if (isnan(value)) {
		return (*env)->NewStringUTF(env, "");
	} else {
		return textify_value_to_jstring(env, sig_digits, value);
	}
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setValueText
 * Signature: (IILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setValueText
  (JNIEnv *env, jclass cls, jint r, jint c, jstring value_str)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col == NULL) {
		goto done;
	}

	if (!col->editable) {
		LOG_WARN("trying to set value to uneditable column");
		goto done;
	}

	float value = NAN;
	if ((*env)->GetStringLength(env, value_str) > 0) {
		const char *buf = (*env)->GetStringUTFChars(env, value_str, JNI_FALSE);
		if (buf == NULL) {
			goto done;
		}
		sscanf(buf, "%g", &value);
		(*env)->ReleaseStringUTFChars(env, value_str, buf);
	}

	if (col->values_len <= r) {
		if (isnan(value)) {
			/* dont write values that are NAN and
			 * require to increase the array
			 */
			goto done;
		}

		if (increase_size_to(col, r + 1) == NULL) {
			THROW_RUNTIME_EXCEPTION(env, "failed to increase array size");
			goto done;
		}
	}

	col->values[r] = value;

	done:
	RELEASE_NATIVE_COLUMNS_LOCK
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getSigDigits
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getSigDigits
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	int value = SIG_DIGITS_AUTOMATIC;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		value = col->sig_digits;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return value;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setSigDigits
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setSigDigits
  (JNIEnv *env, jclass cls, jint c, jint value)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		col->sig_digits = value;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getName
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getName
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(cls);

	const char *ptr = NULL;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		ptr = (const char *)col->name;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return (*env)->NewStringUTF(env, (ptr == NULL) ? "" : ptr);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setName
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setName
  (JNIEnv *env, jclass cls, jint c, jstring value_str)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		const char *buf = (*env)->GetStringUTFChars(env, value_str, JNI_FALSE);
		if (buf == NULL) {
			goto done;
		}

		if (col->name != NULL) {
			free(col->name);
		}

		col->name = (uint8_t *) strdup(buf);

		(*env)->ReleaseStringUTFChars(env, value_str, buf);
	}

	done:
	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getUnit
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getUnit
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(cls);

	const char *ptr = NULL;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		ptr = (const char *)col->unit;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return (*env)->NewStringUTF(env, (ptr == NULL) ? "" : ptr);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setUnit
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setUnit
  (JNIEnv *env, jclass cls, jint c, jstring value_str)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		const char *buf = (*env)->GetStringUTFChars(env, value_str, JNI_FALSE);
		if (buf == NULL) {
			goto done;
		}

		if (col->unit != NULL) {
			free(col->unit);
		}

		col->unit = (uint8_t *) strdup(buf);

		(*env)->ReleaseStringUTFChars(env, value_str, buf);
	}

	done:
	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getColor
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getColor
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	int color = 0xFF000000;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		color = col->color;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return color;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setColor
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setColor
  (JNIEnv *env, jclass cls, jint c, jint value)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		col->color = value;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getLineWidth
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getLineWidth
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	int line_width = 1;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		line_width = col->line_width;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return line_width;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setLineWidth
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setLineWidth
  (JNIEnv *env, jclass cls, jint c, jint value)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		col->line_width = value;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    isEditable
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_isEditable
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	jboolean editable = JNI_FALSE;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		editable = col->editable;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return BOOL_C_TO_JNI(editable);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setEditable
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setEditable
  (JNIEnv *env, jclass cls, jint c, jboolean value)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		col->editable = BOOL_JNI_TO_C(value);
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    setVisible
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_setVisible
  (JNIEnv *env, jclass cls, jint c, jboolean value)
{
	UNUSED(cls);

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		col->visible = BOOL_JNI_TO_C(value);
	}

	RELEASE_NATIVE_COLUMNS_LOCK
	column_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    isVisible
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_isVisible
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	bool visible = false;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col != NULL) {
		visible = col->visible;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	return BOOL_C_TO_JNI(visible);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    getRecentValueText
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_getRecentValueText
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(cls);

	float value = NAN;
	int sig_digits = -1;

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(c);
	if (col == NULL) {
		goto done;
	}

	size_t off = col->values_len;
	while (off > 0) {
		value = col->values[--off];
		if (!isnan(value)) {
			break;
		}
	}

	sig_digits = col->sig_digits;

	done:
	RELEASE_NATIVE_COLUMNS_LOCK

	if (isnan(value)) {
		return (*env)->NewStringUTF(env, "");
	} else {
		return textify_value_to_jstring(env, sig_digits, value);
	}
}
/* __________________________________________________________________ */

static struct {
	jclass cls;
	jmethodID method;
} changed;

/* JNI to have callbacks */
void column_jni_load(JNIEnv *env)
{
	jclass cls = (*env)->FindClass(env,
		"org/madresistor/liab_studio/tunnel/Column");
	if (cls == NULL) {
		LOG_ERROR("unable to FindClass class "
			"org.madresistor.liab_studio.tunnel.Column");
		return;
	} else if ((*env)->ExceptionCheck(env)) {
		LOG_ERROR("Exception occured while FindClass "
			"org.madresistor.liab_studio.tunnel.Column");
		return;
	}

	changed.cls = (*env)->NewGlobalRef(env, cls);
	if (changed.cls == NULL) {
		LOG_ERROR("unable to create a global reference of "
			"class org.madresistor.liab_studio.tunnel.Column");
		return;
	} else if ((*env)->ExceptionCheck(env)) {
		LOG_ERROR("Exception occured while creating "
			"changed.cls global reference");
		return;
	}

	changed.method = (*env)->GetStaticMethodID(env, cls, "changed", "()V");
	if (changed.method == NULL) {
		LOG_ERROR("unable to get method "
			"org.madresistor.liab_studio.tunnel.Column.changed()");
		return;
	} else if ((*env)->ExceptionCheck(env)) {
		LOG_ERROR("Exception occured while get method "
			"org.madresistor.liab_studio.tunnel.Column.changed()");
		return;
	}

	/* color */
	jclass Extra = (*env)->FindClass(env, "org/madresistor/liab_studio/Extra");
	if (cls == NULL) {
		LOG_ERROR("unable to FindClass class "
			"org.madresistor.liab_studio.Extra");
		return;
	} else if ((*env)->ExceptionCheck(env)) {
		LOG_ERROR("Exception occured while FindClass "
			"org.madresistor.liab_studio.Extra");
		return;
	}

	jmethodID newColorForUser =
		(*env)->GetStaticMethodID(env, Extra, "newColorForUser", "()I");
	if (changed.method == NULL) {
		LOG_ERROR("unable to get method "
			"org.madresistor.liab_studio.Extra.newColorForUser()");
		return;
	} else if ((*env)->ExceptionCheck(env)) {
		LOG_ERROR("Exception occured while get method "
			"org.madresistor.liab_studio.Extra.newColorForUser()");
		return;
	}

	ls_column *x = malloc(sizeof(*x));
	if (x == NULL) {
		LOG_ERROR("malloc returned NULL");
		return;
	}

	ls_column *y = malloc(sizeof(*y));
	if (y == NULL) {
		LOG_ERROR("malloc returned NULL");
		return;
	}

	column_defaults_set(x);
	x->name = (uint8_t *) strdup("X");
	x->color = (*env)->CallStaticIntMethod(env, Extra, newColorForUser);
	x->next = y;

	column_defaults_set(y);
	y->name = (uint8_t *) strdup("Y");
	y->color = (*env)->CallStaticIntMethod(env, Extra, newColorForUser);

	native_columns.list = x;
	column_changed(env);
}

void column_jni_unload(JNIEnv *env)
{
	(*env)->DeleteGlobalRef(env, changed.cls);
}

static void* _column_changed(void *arg)
{
	JNIEnv *env = java_start();
	if (env == NULL) {
		LOG_WARN("unable to create JNIEnv variable");
		goto done;
	}

	/* call with non-NULL env */
	column_changed(env);
	java_end();

	done:
	return NULL;
}

/*
 * NOTE: start a new thread that will generate its
 *  own JNIEnv* (if NULL provided), and execute the same method
 */
void column_changed(JNIEnv *env)
{
	if (env == NULL) {
		pthread_t _ign;
		if (pthread_create(&_ign, NULL, _column_changed, NULL) < 0) {
			LOG_DEBUG("unable to start new thread in `column_changed`");
		}

		/* this same function will be called by
		 *   the started thread with env != NULL */
		return;
	}

	if (changed.cls == NULL) {
		LOG_WARN("column changed.cls is NULL");
	} else if (changed.method == NULL) {
		LOG_WARN("column changed.cls is NULL");
	} else {
		(*env)->CallStaticVoidMethod(env, changed.cls, changed.method);
	}
}

static int get_file_descriptor_value(JNIEnv * env, jobject fdObj)
{
	jint fd = -1;

	/* verify the java object */
	if (fdObj == NULL) {
		LOG_WARN("fdObj is NULL");
		goto done;
	}

	/* find the class */
	jclass fdClass = (*env)->FindClass(env, "java/io/FileDescriptor");
	if (fdClass == NULL) {
		LOG_WARN("fdClass (java/io/FileDescriptor) is NULL");
		goto done;
	}

	/* find the filed */
	jfieldID fieldID = (*env)->GetFieldID(env, fdClass, "descriptor", "I");
	if (fieldID == NULL) {
		LOG_WARN("FileDescriptor.descriptor (int) is NULL");
		goto done;
	}

	/* extract fd */
	fd = (*env)->GetIntField(env, fdObj, fieldID);

	done:
	return fd;
}

#define THROW_RUNTIME_EXCEPTION_WITH_ERRNO(msg) { 						\
	char buf[100];														\
	snprintf(buf, sizeof(buf), "%s: %s", msg, strerror(errno));			\
	THROW_RUNTIME_EXCEPTION(env, buf);									\
}

#define EXPORT_CSV_IO_CHECK(code)								\
	if ((code) < 0) {											\
		THROW_RUNTIME_EXCEPTION_WITH_ERRNO(#code);				\
		goto done;												\
	}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    exportCsv
 * Signature: (Ljava/io/FileDescriptor;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_exportCsv
  (JNIEnv *env, jclass cls, jobject _fd)
{
	UNUSED(cls);

	int fd = get_file_descriptor_value(env, _fd);
	if (fd == -1) {
		LOG_WARN("invalid FD");
		return;
	}

	FILE *file = fdopen(fd, "w");
	if (file == NULL) {
		THROW_RUNTIME_EXCEPTION_WITH_ERRNO("fdopen()");
		return;
	}

	ls_column *c;
	unsigned r, row_count = 0;

	HOLD_NATIVE_COLUMNS_LOCK

	/* print the header */
	for (c = native_columns.list; c != NULL; c = c->next) {
		const char *fmt = (c->next != NULL) ? "%s, " : "%s\n";
		EXPORT_CSV_IO_CHECK(fprintf(file, fmt, c->name));
		row_count = MAX(row_count, c->values_len);
	}

	/* print the rows */
	for (r = 0; r < row_count; r++) {
		for (c = native_columns.list; c != NULL; c = c->next) {
			float value = (r < c->values_len) ? c->values[r] : NAN;
			const char *end_with = (c->next != NULL) ? ", " : "\n";

			if (isnan(value)) {
				EXPORT_CSV_IO_CHECK(fputs(end_with, file));
			} else if (c->sig_digits < 0) {
				EXPORT_CSV_IO_CHECK(fprintf(file, "%.7g%s", value, end_with));
			} else {
				EXPORT_CSV_IO_CHECK(fprintf(file, "%#.*g%s", c->sig_digits, value, end_with));
			}
		}
	}

	done:
	RELEASE_NATIVE_COLUMNS_LOCK

	if (fclose(file)) {
		THROW_RUNTIME_EXCEPTION_WITH_ERRNO("fclose()");
		return;
	}
}

static int _csv_is_space(unsigned char c)
{
	return (c == CSV_SPACE || c == CSV_TAB) ? 1 : 0;
}

static int _csv_is_term(unsigned char c)
{
	return (c == CSV_CR || c == CSV_LF) ? 1 : 0;
}

struct _csv_info {
	/* if "row" is -1:
	 *   column_list - linked list of column is build.
	 *   and column is used as last added entry
	 * else:
	 *   used as head to linked list for column. */
	ls_column *column_list;

	/* the target column and row. */
	ls_column *column;
	ssize_t row;

	/* error reporting */
	JNIEnv *env;
	volatile bool raised_exception;
};

static void _csv_cb1_add_column(char *data, struct _csv_info *info)
{
	ls_column *entry = malloc(sizeof(*entry));
	if (entry == NULL) {
		THROW_RUNTIME_EXCEPTION(info->env, "malloc returned NULL");
		info->raised_exception = true;
		return;
	}

	column_defaults_set(entry);

	entry->editable = false; /* prevent editing of column that are readed from file */
	entry->name = (uint8_t *) strdup(data);

	if (info->column_list == NULL) {
		info->column_list = info->column = entry;
	} else {
		info->column->next = entry;
		info->column = entry;
	}
}

static void _csv_cb1_add_cell(char *data, struct _csv_info *info)
{
	/* make sure that we have enough memory to store data */
	if (info->column->values_len <= info->row) {
		size_t inc = MAX(info->column->values_len / 10, 100);
		increase_size_to(info->column, info->column->values_len + inc);
	}

	/* convert data */
	char *endptr;
	float value = strtof(data, &endptr);
	if ((endptr == data) ||
		(value == HUGE_VALF) || (value == -HUGE_VALF) ||
		(value == INFINITY) || (value == -INFINITY)) {
		value = NAN;
	}

	/* store data */
	info->column->values[info->row] = value;

	/* goto next column */
	info->column = info->column->next;
}

static void _csv_cb1(void *_data, size_t len, void *_info)
{
	struct _csv_info *info = _info;
	char *data = _data;

	/* exception was raised */
	if (info->raised_exception) {
		return;
	}

	if (info->row == -1) {
		_csv_cb1_add_column(data, info);
	} else if (info->column != NULL) {
		_csv_cb1_add_cell(data, info);
	}
}

static void _csv_cb2(int c, void *_info)
{
	struct _csv_info *info = _info;

	info->column = info->column_list;
	info->row += 1;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Column
 * Method:    importCsv
 * Signature: (Ljava/io/FileDescriptor;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Column_importCsv
  (JNIEnv *env, jclass cls, jobject _fd)
{
	UNUSED(cls);

	char buf[1024];
	size_t readed;
	struct csv_parser p;
	struct _csv_info info = {
		.column_list = NULL,
		.column = NULL,
		.row = -1,
		.env = env,
		.raised_exception = false
	};

	/* get FD */
	int fd = get_file_descriptor_value(env, _fd);
	if (fd == -1) {
		LOG_WARN("invalid FD");
		return;
	}

	/* open file */
	FILE *file = fdopen(fd, "r");
	if (file == NULL) {
		THROW_RUNTIME_EXCEPTION_WITH_ERRNO("fdopen()");
		return;
	}

	/* initalize csv */
	if (csv_init(&p, CSV_APPEND_NULL)) {
		THROW_RUNTIME_EXCEPTION(env, "csv_init()");
		return;
	}

	csv_set_space_func(&p, _csv_is_space);
	csv_set_term_func(&p, _csv_is_term);

	/* read file */
	while ((readed = fread(buf, 1, 1024, file)) > 0) {
		if (csv_parse(&p, buf, readed, _csv_cb1, _csv_cb2, &info) != readed) {
			snprintf(buf, sizeof(buf), "csv_parse(): %s", csv_strerror(csv_error(&p)));
			THROW_RUNTIME_EXCEPTION(env, buf);
			fclose(file);
			return;
		}

		if (info.raised_exception) {
			csv_free(&p);
			fclose(file);
			return;
		}
	}

	/* done with csv */
	csv_fini(&p, _csv_cb1, _csv_cb2, &info);
	csv_free(&p);

	/* close file */
	if (fclose(file)) {
		THROW_RUNTIME_EXCEPTION_WITH_ERRNO("fclose()");
	}

	/* insert the new columns */
	HOLD_NATIVE_COLUMNS_LOCK
	column_remove_all();
	native_columns.list = info.column_list;
	RELEASE_NATIVE_COLUMNS_LOCK

	curve_changed(env);
	column_changed(env);
}
