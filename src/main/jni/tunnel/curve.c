/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "org_madresistor_liab_studio_tunnel_Curve.h"
#include "curve.h"
#include "column.h"
#include <pthread.h>

struct ls_curve_list native_curves = {
	.lock = PTHREAD_MUTEX_INITIALIZER,
	.list = NULL,
};

ls_curve* curve_get_at(size_t c)
{
	size_t bkp = c;

	ls_curve *i = native_curves.list;

	while (i != NULL) {
		if (c == 0) {
			break;
		}

		c--;
		i = i->next;
	}

	if (i == NULL) {
		LOG_WARN("no such curve: %zu", bkp);
	}

	return i;
}

static void curve_internal_free(ls_curve *crv)
{
	if (crv->name != NULL) {
		free(crv->name);
		crv->name = NULL;
	}

	if (crv->curve != NULL) {
		lp_cartesian_curve_del(crv->curve);
		crv->curve = NULL;
	}
}

/**
 * Remove all curves.
 */
void curve_remove_all()
{
	ls_curve *i = native_curves.list;
	native_curves.list = NULL;

	while (i != NULL) {
		ls_curve *tmp = i;
		i = i->next;

		curve_internal_free(tmp);
		free(tmp);
	}
}

static void curve_defaults_set(ls_curve *crv, ls_column *x, ls_column *y)
{
	crv->column.x = x;
	crv->column.y = y;
	crv->name = NULL;
	crv->color = 0xFF000000;
	crv->line_width = 1;
	crv->show = true;
	crv->next = NULL;
	crv->curve = lp_cartesian_curve_gen();

	lp_cartesian_curve_bool(crv->curve,
		LP_CARTESIAN_CURVE_LINE_SHOW, true);

	lp_cartesian_curve_float(crv->curve,
		LP_CARTESIAN_CURVE_LINE_WIDTH, crv->line_width);

	lp_cartesian_curve_4float(crv->curve,
		LP_CARTESIAN_CURVE_LINE_COLOR, 0, 0, 0, 1);
}

void curve_column_about_to_be_remove(ls_column *col)
{
	ls_curve *i = native_curves.list, *prev = NULL;
	while (i != NULL) {
		if (i->column.x == col || i->column.y == col) {
			if (prev == NULL) {
				native_curves.list = i->next;
			} else {
				prev->next = i->next;
			}

			ls_curve *tmp = i;
			i = i->next;

			curve_internal_free(tmp);
			free(tmp);
		} else {
			prev = i;
			i = i->next;
		}
	}
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    appendCurve
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_appendCurve
  (JNIEnv *env, jclass cls, jint _x, jint _y)
{
	UNUSED(cls);

	size_t off = 0;
	ls_curve *entry = malloc(sizeof(*entry));
	if (entry == NULL) {
		THROW_RUNTIME_EXCEPTION(env, "malloc returned NULL");
		return -1;
	}

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *x = column_get_at(_x);
	ls_column *y = column_get_at(_y);
	if (x == NULL || y == NULL) {
		RELEASE_NATIVE_COLUMNS_LOCK
		free(entry);
		return -1;
	}

	curve_defaults_set(entry, x, y);

	HOLD_NATIVE_CURVES_LOCK

	if (native_curves.list == NULL) {
		native_curves.list = entry;
	} else {
		ls_curve *i = native_curves.list;
		while (i->next != NULL) {
			i = i->next;
			off++;
		}

		off++;
		i->next = entry;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	RELEASE_NATIVE_CURVES_LOCK

	curve_changed(env);

	return (jint) off;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    removeCurve
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_removeCurve
  (JNIEnv *env, jclass cls, jint c)
{
	HOLD_NATIVE_CURVES_LOCK

	size_t off = 0;
	ls_curve *i = native_curves.list, *prev = NULL;

	while (i != NULL) {
		if (off++ == c) {
			break;
		}

		prev = i;
		i = i->next;
	}

	if (i == NULL) {
		LOG_WARN("no such curve: %i", c);
		goto done;
	}

	if (prev == NULL) {
		native_curves.list = i->next;
	} else {
		prev->next = i->next;
	}

	curve_internal_free(i);
	free(i);
	curve_changed(env);

	done:
	RELEASE_NATIVE_CURVES_LOCK
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    removeCurveAll
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_removeCurveAll
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);

	HOLD_NATIVE_CURVES_LOCK

	curve_remove_all();

	RELEASE_NATIVE_CURVES_LOCK

	curve_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    getCurveCount
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_getCurveCount
  (JNIEnv *env, jclass cls)
{
	UNUSED(env); UNUSED(cls);
	size_t count = 0;

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *i = native_curves.list;
	while (i != NULL) {
		i = i->next;
		count++;
	}

	RELEASE_NATIVE_CURVES_LOCK

	return (jint) count;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    getName
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_getName
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(cls);

	const char *ptr = NULL;

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		ptr = (const char *)crv->name;
	}

	RELEASE_NATIVE_CURVES_LOCK

	return (*env)->NewStringUTF(env, (ptr == NULL) ? "" : ptr);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    setName
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_setName
  (JNIEnv *env, jclass cls, jint c, jstring value)
{
	UNUSED(cls);

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv == NULL) {
		goto done;
	}

	const char *buf = (*env)->GetStringUTFChars(env, value, JNI_FALSE);
	if (buf == NULL) {
		goto done;
	}

	if (crv->name != NULL) {
		free(crv->name);
	}

	crv->name = strdup(buf);

	(*env)->ReleaseStringUTFChars(env, value, buf);

	done:
	RELEASE_NATIVE_CURVES_LOCK

	curve_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    getColumnX
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_getColumnX
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	int i = -1;

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		HOLD_NATIVE_COLUMNS_LOCK

		i = column_get_index_of(crv->column.x);

		RELEASE_NATIVE_COLUMNS_LOCK
	}

	RELEASE_NATIVE_CURVES_LOCK

	return i;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    setColumnX
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_setColumnX
  (JNIEnv *env, jclass cls, jint c, jint value)
{
	UNUSED(cls);

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		goto done;
	}

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(value);
	if (col != NULL) {
		crv->column.x = col;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	done:
	RELEASE_NATIVE_CURVES_LOCK
	curve_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    getColumnY
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_getColumnY
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	int i = -1;

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		HOLD_NATIVE_COLUMNS_LOCK

		i = column_get_index_of(crv->column.y);

		RELEASE_NATIVE_COLUMNS_LOCK
	}

	RELEASE_NATIVE_CURVES_LOCK

	return i;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    setColumnY
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_setColumnY
  (JNIEnv *env, jclass cls, jint c, jint value)
{
	UNUSED(cls);

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv == NULL) {
		goto done;
	}

	HOLD_NATIVE_COLUMNS_LOCK

	ls_column *col = column_get_at(value);
	if (col != NULL) {
		crv->column.y = col;
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	done:
	RELEASE_NATIVE_CURVES_LOCK
	curve_changed(env);
}

static void copy_color(lp_color4f *dest, int _color)
{
	uint32_t color = (uint32_t) _color;
	dest->b = ((color >> 0) & 0xFF) / 255.0;
	dest->g = ((color >> 8) & 0xFF) / 255.0;
	dest->r = ((color >> 16) & 0xFF) / 255.0;
	dest->a = ((color >> 24) & 0xFF) / 255.0;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    setColor
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_setColor
  (JNIEnv *env, jclass cls, jint c, jint value)
{
	UNUSED(cls);

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		lp_color4f conv;
		copy_color(&conv, value);
		lp_cartesian_curve_4float(crv->curve,
			LP_CARTESIAN_CURVE_LINE_COLOR, conv.r, conv.g, conv.b, conv.a);
		crv->color = value;
	}

	RELEASE_NATIVE_CURVES_LOCK

	curve_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    getColor
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_getColor
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	int color = 0xFF000000;

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		color = crv->color;
	}

	RELEASE_NATIVE_CURVES_LOCK

	return color;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    setLineWidth
 * Signature: (IF)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_setLineWidth
  (JNIEnv *env, jclass cls, jint c, jfloat value)
{
	UNUSED(cls);

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		crv->line_width = value;
		lp_cartesian_curve_float(crv->curve, LP_CARTESIAN_CURVE_LINE_WIDTH, value);
	}

	RELEASE_NATIVE_CURVES_LOCK

	curve_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    getLineWidth
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_getLineWidth
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	float value = 1;

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		value = crv->line_width;
	}

	RELEASE_NATIVE_CURVES_LOCK

	return value;
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    setVisible
 * Signature: (IZ)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_setVisible
  (JNIEnv *env, jclass cls, jint c, jboolean value)
{
	UNUSED(cls);

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		crv->show = BOOL_JNI_TO_C(value);
	}

	RELEASE_NATIVE_CURVES_LOCK

	curve_changed(env);
}

/*
 * Class:     org_madresistor_liab_studio_tunnel_Curve
 * Method:    isVisible
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_org_madresistor_liab_1studio_tunnel_Curve_isVisible
  (JNIEnv *env, jclass cls, jint c)
{
	UNUSED(env); UNUSED(cls);

	bool show = false;

	HOLD_NATIVE_CURVES_LOCK

	ls_curve *crv = curve_get_at(c);
	if (crv != NULL) {
		show = crv->show;
	}

	RELEASE_NATIVE_CURVES_LOCK

	return BOOL_C_TO_JNI(show);
}

/* __________________________________________________________________ */

static struct {
	jclass cls;
	jmethodID method;
} changed;

/* JNI to have callbacks */
void curve_jni_load(JNIEnv *env)
{
	jclass cls = (*env)->FindClass(env,
		"org/madresistor/liab_studio/tunnel/Curve");
	if (cls == NULL) {
		LOG_ERROR("unable to FindClass class "
			"org.madresistor.liab_studio.tunnel.Curve");
		return;
	} else if ((*env)->ExceptionCheck(env)) {
		LOG_ERROR("Exception occured while FindClass "
			"org.madresistor.liab_studio.tunnel.Curve");
		return;
	}

	changed.cls = (*env)->NewGlobalRef(env, cls);
	if (changed.cls == NULL) {
		LOG_ERROR("unable to create a global reference of "
			"class org.madresistor.liab_studio.tunnel.Curve");
		return;
	} else if ((*env)->ExceptionCheck(env)) {
		LOG_ERROR("Exception occured while creating "
			"changed.cls global reference");
		return;
	}

	changed.method = (*env)->GetStaticMethodID(env, cls, "changed", "()V");
	if (changed.method == NULL) {
		LOG_ERROR("unable to get method "
			"org.madresistor.liab_studio.tunnel.Curve.changed()");
		return;
	} else if ((*env)->ExceptionCheck(env)) {
		LOG_ERROR("Exception occured while get method "
			"org.madresistor.liab_studio.tunnel.Curve.changed()");
		return;
	}
}

void curve_jni_unload(JNIEnv *env)
{
	(*env)->DeleteGlobalRef(env, changed.cls);
}

static void* _curve_changed(void *arg)
{
	JNIEnv *env = java_start();
	if (env == NULL) {
		return NULL;
	}

	/* call with non-NULL env */
	curve_changed(env);
	java_end();
	return NULL;
}

/*
 * NOTE: start a new thread that will generate its
 *  own JNIEnv* (if NULL provided), and execute the same method
 */
void curve_changed(JNIEnv *env)
{
	if (env == NULL) {
		pthread_t _ign;
		if (pthread_create(&_ign, NULL, _curve_changed, NULL) < 0) {
			LOG_DEBUG("unable to start new thread in `curve_changed`");
		}

		/* this same function will be called by
		 *   the started thread with env != NULL */
		return;
	}

	if (changed.cls == NULL) {
		LOG_WARN("curve changed.cls is NULL");
	} else if (changed.method == NULL) {
		LOG_WARN("curve changed.cls is NULL");
	} else {
		(*env)->CallStaticVoidMethod(env, changed.cls, changed.method);
	}
}
