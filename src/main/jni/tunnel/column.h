/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_TUNNEL_COLUMN_H
#define LIAB_STUDIO_TUNNEL_COLUMN_H

#include "../common.h"
#include <stdbool.h>
#include <stdlib.h>
#include <jni.h>
#include <pthread.h>

__BEGIN_DECLS

typedef struct ls_column ls_column;

struct ls_column {
	/* utf-8 */
	uint8_t *name, *unit;
	int sig_digits, color, line_width;
	bool editable, visible /* , deletable */;

	/* volatile to make sure that these dont create any problem */
	size_t values_len;
	float *values;

	ls_column *next;
};

struct ls_column_list {
	/* Protocol: hold "lock" to have exclusive access to "list" */
	pthread_mutex_t lock;
	ls_column *list;
};

extern struct ls_column_list native_columns;

ls_column *column_get_at(size_t c);
ssize_t column_get_index_of(ls_column *col);

void column_changed(JNIEnv *env);

#define HOLD_NATIVE_COLUMNS_LOCK						\
	if (pthread_mutex_lock(&native_columns.lock)) {		\
		LOG_WARN("unable to hold native_column lock");	\
	}

#define RELEASE_NATIVE_COLUMNS_LOCK							\
	if (pthread_mutex_unlock(&native_columns.lock)) {		\
		LOG_WARN("unable to release native_column lock");	\
	}

__END_DECLS

#endif
