/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_TUNNEL_CURVE_H
#define LIAB_STUDIO_TUNNEL_CURVE_H

#include <jni.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "column.h"
#include <libreplot/libreplot.h>

__BEGIN_DECLS

typedef struct ls_curve ls_curve;

struct ls_curve {
	lp_cartesian_curve *curve;
	ls_curve *next;

	struct {
		ls_column *x, *y;
	} column;

	/* just for cache purpose (float rgba) to (int rgba) is not done. */
	int color;
	bool show;
	char *name;
	float line_width;
};

struct ls_curve_list {
	/* Protocol: hold "lock" to have exclusive access to "list" */
	pthread_mutex_t lock;
	ls_curve *list;
};

extern struct ls_curve_list native_curves;

void curve_changed(JNIEnv *env);
void curve_remove_all();
void curve_column_about_to_be_remove(ls_column *col);

#define HOLD_NATIVE_CURVES_LOCK							\
	if (pthread_mutex_lock(&native_curves.lock)) {		\
		LOG_WARN("unable to hold native_curve lock");	\
	}

#define RELEASE_NATIVE_CURVES_LOCK							\
	if (pthread_mutex_unlock(&native_curves.lock)) {		\
		LOG_WARN("unable to release native_curve lock");	\
	}

__END_DECLS

#endif
