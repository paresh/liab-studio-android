/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "org_madresistor_liab_studio_extra_replot_Cartesian.h"
#include "private.h"
#include "cartesian.h"
#include <stdbool.h>
#include <stdlib.h>

#define EXTRACT_ARG(env, obj)	\
	((obj != JNI_NULL) ? EXTRACT_POINTER(env, obj, replot_cartesian) : NULL)

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    onDrawFrame
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_onDrawFrame
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	replot_cartesian *arg = EXTRACT_ARG(env, obj);
	RETURN_IF_NULL(arg);

	glClear(GL_COLOR_BUFFER_BIT);

	lp_cartesian_draw_start(arg->cartesian);
	/* NOTE: use this code and draw curve */
	lp_cartesian_draw_end(arg->cartesian);
}

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    onSurfaceCreated
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_onSurfaceCreated
  (JNIEnv *env, jclass cls, jobject _arg)
{
	UNUSED(env);
	UNUSED(cls);
	UNUSED(_arg);

	lp_init();
}

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    onSurfaceChanged
 * Signature: (Ljava/nio/ByteBuffer;IIFF)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_onSurfaceChanged
  (JNIEnv *env, jclass cls, jobject obj, jint width, jint height, jfloat xdpi, jfloat ydpi)
{
	glViewport(0, 0, width, height);

	replot_cartesian *arg = EXTRACT_ARG(env, obj);
	if (arg == NULL) {
		arg = malloc(sizeof(*arg));
		MEM_ALLOC_CHECK(env, arg, JNI_NULL);

		arg->cartesian = lp_cartesian_gen();
		arg->axis_left = lp_cartesian_axis_gen();
		arg->axis_bottom = lp_cartesian_axis_gen();

		lp_cartesian_pointer(arg->cartesian,
			LP_CARTESIAN_AXIS_AT_LEFT, arg->axis_left);

		lp_cartesian_pointer(arg->cartesian,
			LP_CARTESIAN_AXIS_AT_BOTTOM, arg->axis_bottom);

		lp_cartesian_axis_float(arg->axis_left,
			LP_CARTESIAN_AXIS_VALUE_HEIGHT, 6);

		lp_cartesian_axis_float(arg->axis_bottom,
			LP_CARTESIAN_AXIS_VALUE_HEIGHT, 6);
	}

	lp_cartesian_2float(arg->cartesian, LP_CARTESIAN_DPI, xdpi, ydpi);
	lp_cartesian_4uint(arg->cartesian, LP_CARTESIAN_SURFACE, 0, 0, width, height);
	arg->surface.h = height;
	arg->surface.w = width;

	if (obj == JNI_NULL) {
		obj = (*env)->NewDirectByteBuffer(env, arg, 0);
	}

	return obj;
}

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    onTranslationStart
 * Signature: (Ljava/nio/ByteBuffer;FF)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_onTranslationStart
  (JNIEnv *env, jclass cls, jobject obj, jfloat x_pixel, jfloat y_pixel)
{
	UNUSED(env);
	UNUSED(cls);
	UNUSED(obj);
	UNUSED(x_pixel);
	UNUSED(y_pixel);

	/* TODO: in future, use this to select curve */
}

static void axis_translate(lp_cartesian_axis *axis, unsigned surface, float diff)
{
	float min, max, tmp;
	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	tmp = (max - min) * (diff  / surface);
	min -= tmp;
	max -= tmp;

	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    onTranslationRelative
 * Signature: (Ljava/nio/ByteBuffer;FF)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_onTranslationRelative
  (JNIEnv *env, jclass cls, jobject obj, jfloat dx_pixel, jfloat dy_pixel)
{
	UNUSED(cls);

	replot_cartesian *arg = EXTRACT_ARG(env, obj);
	RETURN_IF_NULL(arg);

	axis_translate(arg->axis_bottom, arg->surface.w, dx_pixel);

	/* -1 to invert Y coordinate */
	axis_translate(arg->axis_left, arg->surface.h, -1 * dy_pixel);
}

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    onTranslationStop
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_onTranslationStop
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(env);
	UNUSED(cls);
	UNUSED(obj);

	/* TODO: in future, use to unselect curve */
}

static void axis_scale(lp_cartesian_axis *axis, float diff)
{
	float min, max, tmp;

	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	tmp = (max - min) * (diff - 1);
	min += tmp;
	max -= tmp;

	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    onScaleRelative
 * Signature: (Ljava/nio/ByteBuffer;FF)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_onScaleRelative
  (JNIEnv *env, jclass cls, jobject obj, jfloat dx, jfloat dy)
{
	UNUSED(cls);

	replot_cartesian *arg = EXTRACT_ARG(env, obj);
	RETURN_IF_NULL(arg);

	axis_scale(arg->axis_bottom, dx);
	axis_scale(arg->axis_left, dy);
}

static void axis_reset(lp_cartesian_axis *axis)
{
	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, 0, 1);
}

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    onDoubleTap
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_onDoubleTap
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	replot_cartesian *arg = EXTRACT_ARG(env, obj);
	RETURN_IF_NULL(arg);

	axis_reset(arg->axis_left);
	axis_reset(arg->axis_bottom);
}

/*
 * Class:     org_madresistor_liab_studio_extra_replot_Cartesian
 * Method:    freeNativeMemory
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_extra_replot_Cartesian_freeNativeMemory
  (JNIEnv *env, jclass cls, jobject obj)
{
	UNUSED(cls);

	replot_cartesian *arg = EXTRACT_ARG(env, obj);
	RETURN_IF_NULL(arg);

	free(arg);
}
