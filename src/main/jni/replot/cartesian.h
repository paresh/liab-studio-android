/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_REPLOT_CARTESIAN_H
#define LIAB_STUDIO_REPLOT_CARTESIAN_H

#include <libreplot/libreplot.h>

__BEGIN_DECLS

struct replot_cartesian {
	lp_cartesian *cartesian;
	lp_cartesian_axis *axis_left;
	lp_cartesian_axis *axis_bottom;
	struct {
		unsigned w, h;
	} surface;
};

typedef struct replot_cartesian replot_cartesian;

__END_DECLS

#endif
