/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "private.h"

/* ref: http://planet.jboss.org/post/pointers_in_jni_c */
/* ref: http://monochrome.sutic.nu/2013/09/01/nice-jni-exceptions.html */

void throw_exception(JNIEnv *env, const char *cls, const char *msg)
{
	jstring sMsg = (*env)->NewStringUTF(env, msg);
	jclass exClass =  (*env)->FindClass (env, cls);
	jmethodID constructor =  (*env)->GetMethodID (env, exClass,
		"<init>", "(Ljava/lang/String;)V");
	jobject exception = (*env)->NewObject (env, exClass, constructor, sMsg);
	(*env)->Throw (env, (jthrowable) exception);
	(*env)->DeleteLocalRef (env, exClass);
}

JavaVM *cachedJVM;

/* cacheed JVM variable for other classes use */
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved)
{
	UNUSED(reserved);
	cachedJVM = jvm;

	JNIEnv *env = java_start();
	if (env != NULL) {
		column_jni_load(env);
		curve_jni_load(env);
		//java_end();
	}

	return JNI_VERSION_1_6;
}

void JNI_OnUnload(JavaVM *vm, void *reserved)
{
	UNUSED(reserved);

	JNIEnv *env = java_start();
	if (env == NULL) {
		return;
	}

	column_jni_unload(env);
	curve_jni_unload(env);

	//java_end();
}

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <android/log.h>
static int pfd[2];
static pthread_t thr;
static const char *tag = "myapp";

static void *thread_func(void*arg)
{
	UNUSED(arg);
    ssize_t rdsz;
    char buf[4096];
    while((rdsz = read(pfd[0], buf, sizeof buf - 1)) > 0) {
        if(buf[rdsz - 1] == '\n') --rdsz;
        buf[rdsz] = 0;  /* add null-terminator */
        __android_log_write(ANDROID_LOG_DEBUG, tag, buf);
    }
    return 0;
}

static int start_logger(const char *app_name)
{
    tag = app_name;

    /* make stdout line-buffered and stderr unbuffered */
    setvbuf(stdout, 0, _IOLBF, 0);
    setvbuf(stderr, 0, _IONBF, 0);

    /* create the pipe and redirect stdout and stderr */
    pipe(pfd);
    dup2(pfd[1], 1);
    dup2(pfd[1], 2);

    /* spawn the logging thread */
    if(pthread_create(&thr, 0, thread_func, 0) == -1)
        return -1;
    pthread_detach(thr);
    return 0;
}


JNIEnv* java_start(void)
{
	JNIEnv *env = NULL;
	int getEnvStat = (*cachedJVM)->GetEnv(cachedJVM, (void **)&env, JNI_VERSION_1_6);
	if (getEnvStat == JNI_EDETACHED) {
		if ((*cachedJVM)->AttachCurrentThread(cachedJVM, &env, NULL) != 0) {
			LOG_WARN("failed to attach");
		}
	} else if (getEnvStat == JNI_OK) {
		// nothing to do
	} else if (getEnvStat == JNI_EVERSION) {
		LOG_WARN("getEnvStat version not supported");
	}

	start_logger("liab-studio-hack");

	return env;
}

void java_end(void)
{
	(*cachedJVM)->DetachCurrentThread(cachedJVM);
}
