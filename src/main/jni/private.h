/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIAB_STUDIO_PRIVATE_H
#define LIAB_STUDIO_PRIVATE_H

#include <jni.h>
#include <stdlib.h>
#include <stddef.h>
#include "common.h"
#include <android/log.h>

__BEGIN_DECLS

#undef UNUSED
#define UNUSED(var) ((void)var)

#define BOOL_C_TO_JNI(v) ((v) ? JNI_TRUE : JNI_FALSE)
#define BOOL_JNI_TO_C(v) ((v) != JNI_FALSE)

#define _LOG_TAG "liab-studio-jni"

#define ANDROID_LOG(type, fmt, ...)										\
	__android_log_print(type,											\
	_LOG_TAG, "%s:%i: " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__)

#if !defined(NDEBUG)
# define LOG_DEBUG(fmt, ...)											\
	ANDROID_LOG(ANDROID_LOG_DEBUG, fmt, ##__VA_ARGS__)
#else
# define LOG_DEBUG(msg, ...)
#endif

#define LOG_WARN(fmt, ...)												\
	ANDROID_LOG(ANDROID_LOG_WARN, fmt, ##__VA_ARGS__)

#define LOG_ERROR(fmt, ...)												\
	ANDROID_LOG(ANDROID_LOG_ERROR, fmt, ##__VA_ARGS__)

#define LOG_DEBUG_INT(variable) LOG_DEBUG(#variable ": %i", variable)

#define THROW_RUNTIME_EXCEPTION(env, msg)								\
	LOG_DEBUG("throw new RuntimeException(\"%s\")", msg);				\
	throw_exception(env, "java/lang/RuntimeException", msg);

#define THROW_INDEX_OUT_OF_BOUNDS_EXCEPTIOn(env, msg)					\
	LOG_DEBUG("throw new IndexOutOfBoundsException(\"%s\")", msg);		\
	throw_exception(env, "java/lang/IndexOutOfBoundsException", msg);

#define THROW_ILLEGAL_ARGUMENT_EXCEPTION(env, msg)					\
	LOG_DEBUG("throw new IllegalArgumentException(\"%s\")", msg);	\
	throw_exception(env, "java/lang/IllegalArgumentException", msg);

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#define EXTRACT_POINTER(env, obj, type)	\
	((type *)(*env)->GetDirectBufferAddress(env, obj))

#define RETURN_IF_NULL(ptr, ...) if (ptr == NULL) { return __VA_ARGS__;}

#define MEM_ALLOC_CHECK(env, ptr, ...)									\
	if (ptr == NULL) {													\
		THROW_RUNTIME_EXCEPTION(env, "memory allocation failed!");		\
		return __VA_ARGS__;												\
	}																	\

#define JNI_NULL 0

void throw_result_exception(JNIEnv *env, int r);
void throw_exception(JNIEnv *env, const char *cls, const char *msg);

extern JavaVM *cachedJVM;
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved);

void column_jni_unload(JNIEnv *env);
void column_jni_load(JNIEnv *env);

void curve_jni_unload(JNIEnv *env);
void curve_jni_load(JNIEnv *env);

/* WARN: only for JNIEnv* created using cachedJVM */
JNIEnv* java_start(void);
void java_end(void);

__END_DECLS

#endif
