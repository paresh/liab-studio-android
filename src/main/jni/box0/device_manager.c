/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "org_madresistor_liab_studio_iface_box0_DeviceManager.h"
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libusb.h>
#include "private.h"

#define EXTRACT_POINTER(env, obj, type)	((type *)(*env)->GetDirectBufferAddress(env, obj))
#define EXTRACT_USBC(env, obj)		EXTRACT_POINTER(env, obj, libusb_context)
#define EXTRACT_USBDH(env, obj)	EXTRACT_POINTER(env, obj, libusb_device_handle)

/*
 * Class:     org_madresistor_liab_studio_iface_box0_DeviceManager
 * Method:    getDeviceHandle
 * Signature: (Ljava/nio/ByteBuffer;Ljava/lang/String;I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_org_madresistor_liab_1studio_iface_box0_DeviceManager_getDeviceHandle
  (JNIEnv *env, jclass cls, jobject _usbc, jstring _path, jint fd)
{
	UNUSED(cls);

	libusb_context *usbc = EXTRACT_USBC(env, _usbc);
	if (usbc == NULL) {
		THROW_RUNTIME_EXCEPTION(env, "context is NULL");
		return 0;
	}

	/* convert java string to native string */
	const char *path = (*env)->GetStringUTFChars(env, _path, NULL);
	if (path == NULL) {
		return 0;
	}

	/* construct a libusb device from path */
	libusb_device *usbd = libusb_get_device2(usbc, path);

	/* free string */
	(*env)->ReleaseStringUTFChars(env, _path, path);

	/* test libusb device */
	if (usbd == NULL) {
		THROW_RUNTIME_EXCEPTION(env, "libusb_get_device2 returned NULL");
		return 0;
	}

	/* contruct a libusb handle from libusb device */
	libusb_device_handle *usbdh;
	if (libusb_open2(usbd, &usbdh, fd) < 0) {
		THROW_RUNTIME_EXCEPTION(env, "libusb_open2 returned error");
		return 0;
	}

	/* return libbox0 device handle */
	return (*env)->NewDirectByteBuffer(env, usbdh, 0);
}

/*
 * Class:     org_madresistor_liab_studio_iface_box0_DeviceManager
 * Method:    freeDeviceHandle
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_iface_box0_DeviceManager_freeDeviceHandle
  (JNIEnv *env, jclass cls, jobject _usbdh)
{
	libusb_device_handle *usbdh = EXTRACT_USBDH(env, _usbdh);

	if (usbdh == NULL) {
		THROW_RUNTIME_EXCEPTION(env, "device handle is NULL");
		return;
	}

	libusb_close(usbdh);
}

/*
 * Class:     org_madresistor_liab_studio_iface_box0_DeviceManager
 * Method:    newContext
 * Signature: ()Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_org_madresistor_liab_1studio_iface_box0_DeviceManager_newContext
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);

	libusb_context *usbc;

	if (libusb_init(&usbc)) {
		THROW_RUNTIME_EXCEPTION(env, "libusb_init failed");
		return 0;
	}

	return (*env)->NewDirectByteBuffer(env, usbc, 0);
}

/*
 * Class:     org_madresistor_liab_studio_iface_box0_DeviceManager
 * Method:    freeContext
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_iface_box0_DeviceManager_freeContext
  (JNIEnv *env, jclass cls, jobject _usbc)
{
	UNUSED(cls);

	libusb_context *usbc = EXTRACT_USBC(env, _usbc);

	if (usbc == NULL) {
		THROW_RUNTIME_EXCEPTION(env, "context is NULL");
		return;
	}

	libusb_exit(usbc);
}
