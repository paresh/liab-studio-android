/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "org_madresistor_liab_studio_iface_box0_source_ain_Logger.h"
#include "org_madresistor_liab_studio_iface_box0_source_Database_Ain.h"
#include <libbox0/libbox0.h>
#include "../private.h"
#include "tunnel/column.h"
#include "tunnel/curve.h"
#include <muParser.h>
#include <pthread.h>
#include <unistd.h>

#define EQUATION org_madresistor_liab_studio_iface_box0_source_Database_Ain_EQUATION
#define PHOTO_GATE org_madresistor_liab_studio_iface_box0_source_Database_Ain_PHOTO_GATE
#define PHOTO_GATE_THRESHOLD org_madresistor_liab_studio_iface_box0_source_Database_Ain_PHOTO_GATE_THRESHOLD

static void* poller(void *arg);
static void feed(float *data, size_t data_len);
static void entries_prepare(JNIEnv *env, jobjectArray _entries, int total, uint8_t *chan_list);
static void entries_free();

#define LOG_RESULT(tag, r) 		\
	LOG_DEBUG("%s: %s => %s", 	\
		tag,					\
		b0_result_name(r),		\
		b0_result_explain(r))

struct entry {
	uint averaging;
	int col_time, col_value;
	uint sample_total, sample_got;

	double averaging_sum;
	uint averaging_index;

	int type;

	union {
		struct {
			float threshold;
			ssize_t trigger_sample;
		} photo_gate;
		struct {
			mu::Parser *parser;
			volatile double output;
		} equation;
	} sensor_specific;
};

struct _entries {
	size_t values_len;
	struct entry *values;
} entries = {
	/* .values_len = */ 0,
	/* .values = */ NULL
};

struct _logger {
	b0_ain *ain;
	double sample_rate;

	size_t count;
	size_t total_count;
	pthread_t thread;
} logger = {
	/* .ain = */ NULL,
	/* .sample_rate = */ 0
};

/* Ref: https://github.com/idstein/FFmpeg/blob/patch-1/compat/bionic/pthread_cancel.h
 * Exit request can be send by sending SIGUSR1 signal */
static void* poller(void *arg)
{
	UNUSED(arg);

	/* block USR1 signal */
	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	pthread_sigmask(SIG_BLOCK, &set, NULL);

	size_t total_fetched = 0;

	/* allocate memory for holding data */
	float *values = (float *) malloc(sizeof(float) * logger.count);
	if (values == NULL) {
		LOG_WARN("malloc returned NULL");
		return NULL;
	}

	for (;;) {
		/* get data */
		size_t has = 0;
		while (has < logger.count) {
			/* read */
			size_t ret;
			if (b0_ain_stream_read_float(logger.ain, values + has,
									logger.count - has, &ret) < 0) {
				LOG_WARN("b0_ain_stream_read_float() failed");
				goto done;
			}

			has += ret;

			/* check if SIGUSR1 is received */
			sigpending(&set);
			if (sigismember(&set, SIGUSR1) == 1) {
				LOG_WARN("ain poller SIGUSR1 recevied, stopping");
				goto done;
			}

			/* sleep for sometime if we got no samples */
			if (!ret) {
				usleep(10000);
			}
		}

		/* process */
		feed(values, logger.count);

		/* update status */
		total_fetched += logger.count;

		/* check if we have recevied the number of samples we need to get */
		if (logger.total_count > 0) {
			if (total_fetched >= logger.total_count) {
				LOG_WARN("reached total_count, stopping");
				break;
			}
		}
	}

	done:
	free(values);
	return NULL;
}

static void feed(float *data, size_t data_len)
{
	size_t i, j;

	if (entries.values == NULL) {
		/* dont proceed, their danger. */
		LOG_WARN("skipping copying since, entries.values == NULL");
		return;
	}

	HOLD_NATIVE_COLUMNS_LOCK

	for (i = 0; i < entries.values_len; i++) {
		struct entry *e = &entries.values[i];
		ls_column *col_time = column_get_at(e->col_time);
		ls_column *col_value = column_get_at(e->col_value);

		if (e->type == EQUATION) {
			/* minimum length alloc.
			 *
			 * since col->values is volatile,
			 *  we dont expect any problem. if problem occur, we can use pthread locks.
			 */
			/* note: ceil */
			size_t div = entries.values_len * e->averaging;
			size_t diff = (data_len + div / 2) / div;
			size_t new_size = col_value->values_len + diff;

			/* preallocate memory */
			/* FIX: update memory */
			col_time->values = (float *)realloc(col_time->values, new_size * sizeof(float));
			col_value->values = (float *)realloc(col_value->values, new_size * sizeof(float));
		}

		/* copy */
		for (j = i; j < data_len; j += entries.values_len) {

			/* limit the number of samples to take */
			if (e->sample_total > 0) {
				if (e->sample_got >= e->sample_total) {
					/* we have reached the limit */
					break;
				}
			}

			/* averaging */
			e->averaging_sum += data[j];
			e->averaging_index++;

			/* perform averaging */
			if (e->averaging_index >= e->averaging) {
				float calc_value = e->averaging_sum / e->averaging_index;

				switch(e->type) {
				case EQUATION: {
					size_t k = col_value->values_len;

					/* transfer function
					 * NOTE: not expecting any problem here */
					if (e->sensor_specific.equation.parser != NULL) {
						e->sensor_specific.equation.output = calc_value;
						try {
							calc_value = e->sensor_specific.equation.parser->Eval();
						} catch (mu::Parser::exception_type &e) {
							//wtf
							calc_value = NAN;
						}
					}

					col_time->values[k] = k / logger.sample_rate;
					col_value->values[k] = calc_value;
					col_time->values_len = col_value->values_len = k + 1;
				} break;
				case PHOTO_GATE:
					if (calc_value < e->sensor_specific.photo_gate.threshold) {
						if (e->sensor_specific.photo_gate.trigger_sample < 0) {
							e->sensor_specific.photo_gate.trigger_sample = e->sample_got;
						}
					} else {
						if (e->sensor_specific.photo_gate.trigger_sample >= 0) {
							size_t k = col_value->values_len;

							/* calculation */
							size_t trigger_sample = e->sensor_specific.photo_gate.trigger_sample;
							size_t diff = e->sample_got - trigger_sample;
							e->sensor_specific.photo_gate.trigger_sample = -1;

							/* FIX: update memory */
							size_t new_size = k + 1;
							col_time->values = (float *) realloc(col_time->values, new_size * sizeof(float));
							col_value->values = (float *) realloc(col_value->values, new_size * sizeof(float));

							/* write data in columns */
							col_time->values[k] = trigger_sample / logger.sample_rate;
							col_value->values[k] = diff / logger.sample_rate;

							/* update length */
							col_time->values_len = col_value->values_len = new_size;
						}
					}
				break;
				}

				e->averaging_sum = 0;
				e->averaging_index = 0;
			}

			e->sample_got++;
		}
	}

	RELEASE_NATIVE_COLUMNS_LOCK

	column_changed(NULL);
	curve_changed(NULL);
}

static void entries_prepare(JNIEnv *env, jobjectArray _entries, int total, uint8_t *chan_list)
{
	entries.values_len = env->GetArrayLength(_entries);
	entries.values = (struct entry *) malloc(entries.values_len * sizeof(*entries.values));
	size_t i;
	/* FIXME: check malloc return */

	const char *cls_name = "org/madresistor/liab_studio/iface/box0/source/ain/Logger$Entry";
	jclass cls = env->FindClass(cls_name);

	jfieldID field_channel = env->GetFieldID(cls, "channel", "S");
	jfieldID field_averaging = env->GetFieldID(cls, "averaging", "I");
	jfieldID field_transferFunction = env->GetFieldID(cls, "transferFunction", "Ljava/lang/String;");
	jfieldID field_col_time = env->GetFieldID(cls, "col_time", "I");
	jfieldID field_col_value = env->GetFieldID(cls, "col_value", "I");
	jfieldID field_col_type = env->GetFieldID(cls, "type", "I");

	for (i = 0; i < entries.values_len; i++) {
		jobject _e = env->GetObjectArrayElement(_entries, i);
		struct entry *e = &entries.values[i];

		/* channel */
		chan_list[i] = (uint8_t) env->GetShortField(_e, field_channel);

		/* averaging */
		e->averaging = env->GetIntField(_e, field_averaging);
		if (e->averaging <= 0) {
			e->averaging = 1;
		}
		e->averaging_index = 0;
		e->averaging_sum = 0;

		/* columns */
		e->col_time = env->GetIntField(_e, field_col_time);
		e->col_value = env->GetIntField(_e, field_col_value);

		/* type */
		e->type = env->GetIntField(_e, field_col_type);

		switch(e->type) {
		case PHOTO_GATE:
			e->sensor_specific.photo_gate.threshold = PHOTO_GATE_THRESHOLD;
			e->sensor_specific.photo_gate.trigger_sample = -1;
		break;
		case EQUATION: {
			/* transfer function */
			e->sensor_specific.equation.parser = NULL;

			jstring tFunc = (jstring) env->GetObjectField(_e, field_transferFunction);
			if (tFunc != 0) {
				if (env->GetStringUTFLength(tFunc) > 0) {
					e->sensor_specific.equation.parser = new mu::Parser();
					const char *eq = env->GetStringUTFChars(tFunc, NULL);
					e->sensor_specific.equation.parser->SetExpr(eq);
					env->ReleaseStringUTFChars(tFunc, eq);
					double *output = (double *) &e->sensor_specific.equation.output;
					e->sensor_specific.equation.parser->DefineVar("value", output);
				}
			}
			env->DeleteLocalRef(tFunc);
		} break;
		}

		/* total samples to accumlate */
		e->sample_total = total / entries.values_len;
		e->sample_got = 0;

		/* unref the _e */
		env->DeleteLocalRef(_e);
	}
}

static void entries_free()
{
	size_t i;
	for (i = 0; i < entries.values_len; i++) {
		struct entry *e = &entries.values[i];
		if (e->type == EQUATION) {
			if (e->sensor_specific.equation.parser != NULL) {
				delete e->sensor_specific.equation.parser;
			}
		}
	}

	/* free if their is any entries unfreed */
	if (entries.values != NULL) {
		free(entries.values);
	}

	entries.values_len = 0;
	entries.values = NULL;
}

/*
 * Class:     org_madresistor_liab_studio_iface_box0_source_ain_Logger
 * Method:    start
 * Signature: (Ljava/nio/ByteBuffer;[Lorg/madresistor/liab_studio/iface/box0/source/ain/Logger/Entry;ILjava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_iface_box0_source_ain_Logger_start
  (JNIEnv *env, jclass cls, jobject _ain, jobjectArray _entries, jint time, jobject _stream_value)
{
	b0_result_code r;
	b0_ain *ain = ((b0_ain *) env->GetDirectBufferAddress(_ain));

	b0_stream_value *stream_value = ((b0_stream_value *)
		env->GetDirectBufferAddress(_stream_value));

	entries_free();

	/* FIXME: chan_list is hardcoded to max 255 */
	uint8_t chan_list[255];
	entries_prepare(env, _entries, time * stream_value->speed, chan_list);

	/* poller parameters */
	size_t count = stream_value->speed / 4;
	logger.count = count - (count % entries.values_len);
	logger.total_count = stream_value->speed * time;
	logger.ain = ain;
	logger.sample_rate = stream_value->speed;

	/* prepare */
	r = b0_ain_stream_prepare(ain, stream_value);
	if (B0_ERR_RC(r)) {
		LOG_DEBUG("error while ain prepare: %s => %s",
				b0_result_name(r), b0_result_explain(r));
		goto error;
	}

	/* channel sequence */
	r = b0_chan_seq_set(ain->chan_seq, chan_list, entries.values_len);
	if (B0_ERR_RC(r)) {
		LOG_DEBUG("error while chan seq set: %s => %s",
				b0_result_name(r), b0_result_explain(r));
		goto error;
	}

	/* start it */
	r = b0_ain_stream_start(ain);
	if (B0_ERR_RC(r)) {
		LOG_DEBUG("error while ain start: %s => %s",
				b0_result_name(r), b0_result_explain(r));
		goto error;
	}

	/* start poller thread */
	if (pthread_create(&logger.thread, NULL, poller, NULL) < 0) {
		LOG_WARN("unable to start poller");
		goto error;
	}

	return 1;

	error:
	entries_free();
	logger.ain = NULL;
	return -1;
}

/*
 * Class:     org_madresistor_liab_studio_iface_box0_source_ain_Logger
 * Method:    stop
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_madresistor_liab_1studio_iface_box0_source_ain_Logger_stop
  (JNIEnv *env, jclass cls)
{
	UNUSED(cls);
	UNUSED(env);

	if ((entries.values == NULL) && (logger.ain == NULL)) {
		return -1;
	}

	if (logger.ain != NULL) {
		/* request poller to exit */
		if (pthread_kill(logger.thread, SIGUSR1)) {
			LOG_WARN("unable to cancel poller thread");
			return -1;
		}

		/* wait for the poller to exit */
		if (pthread_join(logger.thread, NULL)) {
			LOG_WARN("unable to join thread");
			return -1;
		}

		b0_result_code r = b0_ain_stream_stop(logger.ain);
		logger.ain = NULL;

		if (B0_ERR_RC(r)) {
			LOG_DEBUG("error while ain stop: %s => %s",
				b0_result_name(r), b0_result_explain(r));
			return -1;
		}
	}

	entries_free();

	return 1;
}
