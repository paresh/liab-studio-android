/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "org_madresistor_liab_studio_iface_box0_source_Database.h"
#include "private.h"
#include <muParser.h>

char* transfer_func_test(const char *eq);

/* returned string (if not NULL) is freed by caller */
char* transfer_func_test(const char *eq)
{
	mu::Parser *parser = new mu::Parser();
	volatile double output = 0;
	char *result = NULL;

	try {
		parser->DefineVar("output", (double *)&output);
		parser->SetExpr(eq);
		volatile double eval = parser->Eval();
		LOG_DEBUG("function %s resulted %f for output = 0", eq, eval);
	} catch (mu::Parser::exception_type &e) {
		const char *msg = e.GetMsg().c_str();
		const char *expr = e.GetExpr().c_str();
		const char *token = e.GetToken().c_str();
		int pos = e.GetPos();
		int code =  e.GetCode();
		char buf[1000];
		snprintf(buf, sizeof(buf),
			"MuParser (the equation solving library) has returned error while evaluating the transfter function.\n"
			"Message: %s\n"
			"Formula: %s\n"
			"Token: %s\n"
			"Position: %i\n"
			"Errc: %i",
			msg, expr, token, pos, code);

		/* store the message in result */
		result = strdup(buf);
	}

	delete parser;
	return result;
}

/*
 * Class:     org_madresistor_liab_studio_iface_box0_source_Database
 * Method:    transferFuncTest
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_madresistor_liab_1studio_iface_box0_source_Database_transferFuncTest
  (JNIEnv *env, jclass cls, jstring _eq)
{
	UNUSED(cls);

	const char *eq = env->GetStringUTFChars(_eq, NULL);
	char *result = transfer_func_test(eq);
	env->ReleaseStringUTFChars(_eq, eq);

	if (result != NULL) {
		jstring _result = env->NewStringUTF(result);
		free(result);
		return _result;
	}

	return NULL;
}
