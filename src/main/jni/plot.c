/*
 * This file is part of liab-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeepdhaka9@gmail.com>
 *
 * liab-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liab-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with liab-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "org_madresistor_liab_studio_widget_graph_PlotView.h"
#include "../tunnel/curve.h"
#include "../tunnel/column.h"
#include "replot/cartesian.h"
#include "../private.h"

#define EXTRACT_ARG(env, obj)	\
	((obj != JNI_NULL) ? EXTRACT_POINTER(env, obj, replot_cartesian) : NULL)

/* NOTE: the upper limit for plotting recent samples.
 * GPU [Adreno 305 - MotoG] took 96000 samples per second for about 30sec
 *  that means about 28 Lakhs points (not bad!) */
#define SAMPLE_COUNT_LIMIT (300000)

static void set_data(ls_curve *crv, lp_cartesian_curve_data_axis axis,
		ls_column *col, size_t skip)
{
	lp_cartesian_curve_data(crv->curve, axis,
		0, GL_FLOAT, sizeof(float),
		&col->values[skip], col->values_len - skip);
}

/**
 * Draw the curves.
 * - lock the x,y columns
 * - use the pointer of columns to data
 * - copy the count
 */
static void draw_curve(lp_cartesian *cartesian, ls_curve *crv)
{
	if (!crv->show) {
		return;
	}

	ls_column *x = crv->column.x;
	ls_column *y = crv->column.y;

	if ((x == NULL) || (y == NULL)) {
		LOG_WARN("column x or y is NULL");
		return;
	}

	size_t skip = 0;

	/* limit the values.
	 *  GPU have finite resources and processing power.
	 *  (very relative count, depends on Hardware)
	 *  if lots of samples are plotted,
	 *   laggyness is seen as well and application stability */
	size_t count = MIN(x->values_len, y->values_len);
	if (count > SAMPLE_COUNT_LIMIT) {
		skip = count - SAMPLE_COUNT_LIMIT;
	}

	set_data(crv, LP_CARTESIAN_CURVE_X, x, skip);
	set_data(crv, LP_CARTESIAN_CURVE_Y, y, skip);

	lp_cartesian_draw_curve(cartesian, crv->curve);
}

/*
 * Class:     org_madresistor_liab_studio_widget_graph_PlotView
 * Method:    onDrawFrame
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_madresistor_liab_1studio_widget_graph_PlotView_onDrawFrame
  (JNIEnv *env, jclass cls, jobject _arg)
{
	UNUSED(cls);

	glClear(GL_COLOR_BUFFER_BIT);

	replot_cartesian *arg = EXTRACT_ARG(env, _arg);
	RETURN_IF_NULL(arg);

	HOLD_NATIVE_COLUMNS_LOCK

	HOLD_NATIVE_CURVES_LOCK

	lp_cartesian_draw_start(arg->cartesian);

	ls_curve *crv;
	for (crv = native_curves.list; crv != NULL; crv = crv->next) {
		draw_curve(arg->cartesian, crv);
	}

	lp_cartesian_draw_end(arg->cartesian);

	RELEASE_NATIVE_CURVES_LOCK

	RELEASE_NATIVE_COLUMNS_LOCK
}
